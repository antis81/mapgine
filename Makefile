BASS = bass
OBUILD = -i mapengine.asm -s build/bass.sym -l build/bass.src
C64 = x64sc

all: engine

prebuild:
	mkdir -p build

#minapi: prebuild
# TODO: build minimal BASIC api

# api: prebuild
# TODO: build full BASIC api

#tests:

engine: prebuild
	${BASS} ${OBUILD} -o build/mapgine.prg -DHEIGHTMAP -DHM_VERTICALS

run: engine
	echo "starting ${C64}"
	${C64} build/mapgine.prg

clean :
	rm -rf build/*
