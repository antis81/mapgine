!print "memmap imported"

!ifdef NESTED_INCLUDE {
  !include "macros/mem.asm"
}

; public variables (parameters) and constants
!section "vars", $0340, NoStore=true
vars
  .draw_mode: MEM_INT8()   ; (uint8)  see macro SET_DRAW_MODE
  .x0: MEM_INT16()         ; (uint16) x0 vertex coordinate
  .y0: MEM_INT16()         ; (uint16) y0 vertex coordinate
  .z0: MEM_INT16()         ; (uint16) z0 vertex coordinate
  .x1: MEM_INT16()         ; (uint16) x1 vertex coordinate
  .y1: MEM_INT16()         ; (uint16) y1 vertex coordinate
  .z1: MEM_INT16()         ; (uint16) z1 vertex coordinate
  ; $034d-$034f unused

  !ifdef HEIGHTMAP {
    !align $10
    ; * = $0350 "heightmap_vars" virtual
    .heightmap_field_width: MEM_INT8() ; (uint8) heightmap stretch factor (0..255)
  }

  .end = *-1
  .size = .end - vars + 1
  !print "variables: ", vars, "-", .end, " (", str(.size), " bytes)"


!ifdef BASIC_API {
  ; cut-off basic ram from $800-$9fff
  !section "basic_ram", start=MEM.BASIC_RAM, size=$1fff, NoStore=true
  basic_ram:
    .end = *-1
    !print MemArrayStr("basic_ram", basic_ram, .end)
}

; color ram (1024 bytes)
!section "color_ram", start=$c000, size=$0400, NoStore=true
  color_ram: MEM_ARRAY($0400)
  .end = *-1
  !print MemArrayStr("color_ram", color_ram, .end)

; video ram (8000 bytes)
!section "vram", start=$e000, size=8000, NoStore=true
  vram: !fill 8000, 0
  .end = *-1
  .size = .end - vram +1
  !print MemArrayStr("vram", vram, .end)

; tables generated at runtime
!section "r_tables", start=(color_ram.end+1), align=$100, NoStore=true
!align $100
r_tables
  !print MemArrayStr("r_tables", r_tables, r_tables.end)

  .gfx_mmtab: MEM_ARRAY(640)
  .gfx_mmtab_end = *-1
  !print "  ", MemArrayStr("r_tables.gfx_mmtab", .gfx_mmtab, .gfx_mmtab_end)

  .gfx_end = *-1

  !align $100
  .math_square0:
  .math_square0_lo: MEM_ARRAY(512)
  .math_square0_hi: MEM_ARRAY(512)
  .math_square0_end = *-1
  !print "  ", MemArrayStr("r_tables.math_square0", .math_square0, .math_square0_end)

  .math_square1:
  .math_square1_lo: MEM_ARRAY(512)
  .math_square1_hi: MEM_ARRAY(512)
  .math_square1_end = *-1
  !print "  ", MemArrayStr("r_tables.math_square1", .math_square1, .math_square1_end)

  .math_end = *-1

  !ifdef HM_OFFSETS {
    .heightmap_field_offsets:
    .heightmap_field_offsets_lo: MEM_ARRAY(heightmap.width)
    .heightmap_field_offsets_hi: MEM_ARRAY(heightmap.width)
    .heightmap_field_offsets_end = *-1
    !print "  ", MemArrayStr("r_tables.heightmap_field_offsets", .heightmap_field_offsets, .heightmap_field_offsets_end)
  }
  !ifdef HM_REVOFFSETS {
    .heightmap_field_revoffsets:
    .heightmap_field_revoffsets_lo: MEM_ARRAY(heightmap.width)
    .heightmap_field_revoffsets_hi: MEM_ARRAY(heightmap.width)
    .heightmap_field_revoffsets_end = *-1
    !print "  ", MemArrayStr("r_tables.heightmap_revfield_offsets", .heightmap_field_revoffsets, .heightmap_field_revoffsets_end)
  }
  .end = *-1


!ifdef HEIGHTMAP {
  !section "heightmap", start=$9000, size=heightmap.size, NoStore=true
  heightmap:
    .depth   = 16               ; number of rows drawn
    .columns = 9                ; number of columns drawn
    .width   = 64               ; width & height of the heightmap (32==1k, 64==4k, …)
    .size    = .width * .width  ; size in bytes
    MEM_ARRAY(.size)
    .end = *-1
    !print MemArrayStr("heightmap", heightmap, .end)
}


!ifdef BASIC_FAC {
  ; !section "fac_vars", r_tables.gfx_mmtab_end+1, NoStore=true
  ; fac_vars:
  ;   ; addresses for BASIC float coordinates
  ;   ; ._mem_floats_begin = MMTAB_END + 1
  ;   .f_temp: MEM_FAC_FLOAT() ; 5 byte
  ;   .f_x1:            MEM_FAC_FLOAT() ; 5 byte
  ;   .f_x0:            MEM_FAC_FLOAT() ; 5 byte
  ;   .f_y1:            MEM_FAC_FLOAT() ; 5 byte
  ;   .f_y0:            MEM_FAC_FLOAT() ; 5 byte
  ;   .f_zero:          MEM_FAC_FLOAT() ; 5 byte -> const 0
  ;   .f_screen_width:  MEM_FAC_FLOAT() ; 5 byte -> const 320
  ;   .f_screen_bottom: MEM_FAC_FLOAT() ; 5 byte -> const 200
  ;   .result_code:     MEM_FAC_FLOAT() ; 1 byte -> result code
  ;   .end = *-1
  ;   .size = .end - fac_vars + 1
  ; !print "floats   : ", _mem_floats_begin,4), "-$", str(_mem_floats_end,4), " (", _mem_floats_size, " bytes)"
  ; ; 8*5 + 1 = 41 bytes
}

; internal (private) variables (use zeropage as much as possible)
!section "zp", start=$0000, size=$0100, NoStore=true
                                  ; $00-$01 (CPU register)
  * = ZP02_UNUSED ;"zp_02" virtual ; $02
  _tmp8_0: MEM_INT8()

  * = ZP03_BASIC ;"zp_03" virtual
  _tmp16_0: MEM_INT16()           ; $03-$04
  _tmp16_1: MEM_INT16()           ; $05-06

  ; $05-$13 (TBD)

  * = ZP14_BASIC ;"zp_14" virtual
  _tmp16_2: MEM_INT16()           ; $14-$15

  ; $16-$25 (TBD)

  * = ZP26_FAC ;"zp_26" virtual
  _tmp16_3: MEM_INT16()           ; $26-$27

  ; $28-$60 (TBD)

  * = ZP61_FAC ;"zp_61" virtual
  _tmp32_0: MEM_INT32()           ; $61-$64
  _tmp32_1: MEM_INT32()           ; $65-$68
  _tmp32_2: MEM_INT32()           ; $69-$6c
  _tmp32_3: MEM_INT32()           ; $6d-$70
  _tmp16_4: MEM_INT16()           ; $71-$72

  ; $73-$ff

; zp aliases
_ptr0            = _tmp16_0
_dx              = _tmp16_1
_bh_err          = _tmp16_2   ; bresenham error variable
_ix              = _tmp16_3

_dy              = _tmp32_0+0 ; !SHARED! -> used as multiplication product!
_adv_h           = _tmp32_0+1 ; !SHARED! -> used as multiplication product!
_line_is_steep   = _tmp32_0+2 ; !SHARED! -> used as multiplication product!
_iy              = _tmp32_0+3 ; !SHARED! -> used as multiplication product!

_y0              = _tmp32_1
_y1              = _tmp32_1+1
_bh_loop_idx     = _tmp32_1+2 ; 16bit

_x0              = _tmp32_2   ; 16bit
_x1              = _tmp32_2+2 ; 16bit

_heightmap_row0  = _tmp32_3
_heightmap_row1  = _tmp32_3+1
_heightmap_col   = _tmp32_3+2
_draw_mode       = _tmp32_3+3


!macro _INIT_DRAW_MODE() {
  lda vars.draw_mode
  sta _draw_mode
}
