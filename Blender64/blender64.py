#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json

from PySide6.QtCore import Qt, Slot, QObject
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtGui import QGuiApplication
from __feature__ import snake_case, true_property


class Formatter(QObject):
    @Slot(int, int)
    def hexStr(self, num: int, padding: int) -> str:
      return f'{x2:num}'

    @Slot(int, int)
    def pad_num(self, num: int, padding: int) -> str:
        return str(num).zfill(padding)


if __name__ == '__main__':
    app = QGuiApplication([])
    app.organization_name = 'CubeDemo'
    app.organization_domain = 'cubedemo.org'

    eng = QQmlApplicationEngine()
    eng.quit.connect(app.quit)
    py_fmt = Formatter()
    #eng.set_initial_properties({'py_fmt': py_fmt})
    eng.load("blender64.qml")

    sys.exit(app.exec())
