import QtQuick 2.0
import QtQuick.Controls 2.15

Label {
  property string prefix: ''
  property int value: undefined

  id: root
  width:  64
  height: width
  background: Rectangle{
    color: color_for_int8()
    border { color: "#ccc" }
  }
  text: {
    if (root.prefix) {
      return prefix + ': ' + root.value_str()
    }
    return root.value_str()
  }
  horizontalAlignment: Qt.AlignHCenter
  verticalAlignment: Qt.AlignVCenter

  function color_for_int8() {
    if (root.value < -128) {
      return "#e66"
    }
    if (root.value > 127) {
      return "#e66"
    }

    // value ok
    return "transparent"
  }

  function value_str() {
    let s = root.value.toString()
    if (s.startsWith('-')) {
      // TODO: prefix negative values
      return s
    }

    let s_len = s.length
    let s_padding = 3 - s_len
    let res = ''
    for (let i=0; i<s_padding; i++) {
      res += '0'
    }
    res += s
    return res
  }

  MouseArea {
    id: tooltip_area
    anchors.fill: parent
    hoverEnabled: true
  }

  ToolTip {
    implicitWidth: 64
    text: {
      let v = value
      if (v<0) {
        v += 256  // int8 (two's complement)
      }
      let s = '0x'
      if (v<10) {
        s += '0'
      }
      s += v.toString(16)
    }
    visible: tooltip_area.containsMouse
  }
}
