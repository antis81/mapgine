import QtQuick 2.0

Column {
  id: root;

  property RMatrix rmatrix: null

  Row{ spacing: 0; RMatrixValue{ prefix: "A"; value: rmatrix.a } RMatrixValue{ prefix: "B"; value: rmatrix.b } RMatrixValue{ prefix: "C"; value: rmatrix.c } }
  Row{ spacing: 0; RMatrixValue{ prefix: 'D'; value: rmatrix.d } RMatrixValue{ prefix: "E"; value: rmatrix.e } RMatrixValue{ prefix: "F"; value: rmatrix.f } }
  Row{ spacing: 0; RMatrixValue{ prefix: 'G'; value: rmatrix.g } RMatrixValue{ prefix: "H"; value: rmatrix.h } RMatrixValue{ prefix: "I"; value: rmatrix.i } }
}
