import QtQuick 2.0

VertexObject {
  id: self
  num_vertices: 8
  Component.onCompleted: {
    // cube vertices
    let l = self.l/2
    x[0] = -l; y[0] = -l; z[0] = -l // front-left-bottom
    x[1] = -l; y[1] = -l; z[1] =  l // front-left-top
    x[2] =  l; y[2] = -l; z[2] =  l // front-right-top
    x[3] =  l; y[3] = -l; z[3] = -l // front-right-bottom
    x[4] = -l; y[4] =  l; z[4] = -l // back-left-bottom
    x[5] = -l; y[5] =  l; z[5] =  l // back-left-top
    x[6] =  l; y[6] =  l; z[6] =  l // back-right-top
    x[7] =  l; y[7] =  l; z[7] = -l // back-right-bottom

    seal_vertices()
 }

  property int ox: 0
  property int oy: 0

  /** rotates the cube by using a rotation matrix
  @param m      (int8 array) rotation matrix
  @param m_neg  (int8 array) negated rotation matrix
  */
  function matrix_transform(m:RMatrix, m_neg:RMatrix, t_yfact:Array) {
    let xd = self.xd
    let yd = self.yd
    let zd = self.zd
    let vx = self.vx
    let vy = self.vy

    const A = m.a
    const B = m.b
    const C = m.c
    const D = m.d
    const E = m.e
    const F = m.f
    const G = m.g
    const H = m.h
    const I = m.i

    for (let np=0; np < self.x.length; np++) {
      const x = self.x[np]
      const y = self.y[np]
      const z = self.z[np]

      xd[np] = (z*A + x*B + y*C)/127  // x' = z*A + x*B + y*C
      yd[np] = (z*D + x*E + y*F)/127  // y' = z*D + x*E + y*F
      zd[np] = (z*G + x*H + y*I)/127  // z' = z*G + x*H + y*I

      // project to view coordinates (C64 resolution: 320x200)
      let depth_idx = yd[np]
      if (depth_idx<0) {
        depth_idx = 256 - Math.abs(depth_idx)  //two's complement
      }
      const depth = t_yfact[depth_idx]

      vx[np] = xd[np] * depth
      vy[np] = zd[np] * depth
    }

    //console.debug('rotated cube x -> ' + xd)
    //console.debug('rotated cube y -> ' + yd)
    //console.debug('rotated cube z -> ' + zd)

    //console.debug("projected cube view_x -> " + vx)
    //console.debug("projected cube view_y -> " + vy)
  }

  /** render the cube */
  function transform(co, si, nf, pf, rx, ry, rz) {
    const offset = 64
    const cx = co[rx]; const sx = si[rx]
    const cy = co[ry]; const sy = si[ry]
    const cz = co[rz]; const sz = si[rz]
    let xt = 0; let yt = 0; let zt = 0

    const x = self.x
    const y = self.y
    const z = self.z
    let xd = self.xd
    let yd = self.yd
    let zd = self.zd
    const num_vertices = self.x.length // -> 8

    for (let np=0; np<num_vertices; np++) {
      // rotation on x axes
      yt = y[np]
      yd[np] = Math.trunc((cx*yt - sx*z[np])/offset)
      zd[np] = Math.trunc((sx*yt + cx*z[np])/offset)

      // rotation on y axes
      xt = x[np]
      zt = self.zd[np]
      xd[np] = Math.trunc((cy*xt + sy*zt)/offset)
      zd[np] = Math.trunc((cy*zt - sy*xt)/offset)

      // rotation on z axes
      xt = self.xd[np]
      yt = self.yd[np]
      xd[np] = Math.trunc((cz*xt - sz*yt)/offset)
      yd[np] = Math.trunc((sz*xt + cz*yt)/offset)

      // point projections and translations to screen coordinates
      const ydt = Math.abs(z[np])
      if (self.zd[np] < 0) {
        vx[np] = Math.trunc(xd[np] * nf[ydt]/offset)
        vy[np] = Math.trunc(yd[np] * nf[ydt]/offset)
        //console.debug("projected cube x "+np+" -> " + self.vx)
        //console.debug("projected cube y "+np+" -> " + self.vy)
      }else{
        vx[np] = Math.trunc(xd[np] * pf[ydt]/offset)
        vy[np] = Math.trunc(yd[np] * pf[ydt]/offset)
        //console.debug("projected cube x "+np+" -> " + self.vx)
        //console.debug("projected cube y "+np+" -> " + self.vy)
      }

      // example without the rotation
      // vx[np] = Math.trunc(x[np]*fs/(y[np]+fs))
      // vy[np] = Math.trunc(z[np]*fs/(y[np]+fs))
    }
  }

  function render(ctx) {
    const ox = self.ox; const oy = self.oy
    let vx0 = vx[0]+ox; let vy0 = vy[0]+oy
    let vx1 = vx[1]+ox; let vy1 = vy[1]+oy
    let vx2 = vx[2]+ox; let vy2 = vy[2]+oy
    let vx3 = vx[3]+ox; let vy3 = vy[3]+oy
    let vx4 = vx[4]+ox; let vy4 = vy[4]+oy
    let vx5 = vx[5]+ox; let vy5 = vy[5]+oy
    let vx6 = vx[6]+ox; let vy6 = vy[6]+oy
    let vx7 = vx[7]+ox; let vy7 = vy[7]+oy

    ctx.beginPath()
    // cube front
    ctx.moveTo(vx0, vy0); ctx.lineTo(vx1, vy1); ctx.lineTo(vx2, vy2); ctx.lineTo(vx3, vy3); ctx.lineTo(vx0, vy0)
    // cube back
    ctx.moveTo(vx4, vy4); ctx.lineTo(vx5, vy5); ctx.lineTo(vx6, vy6); ctx.lineTo(vx7, vy7); ctx.lineTo(vx4, vy4)
    // cube edges
    ctx.moveTo(vx0, vy0); ctx.lineTo(vx4, vy4)
    ctx.moveTo(vx1, vy1); ctx.lineTo(vx5, vy5)
    ctx.moveTo(vx3, vy3); ctx.lineTo(vx7, vy7)
    ctx.moveTo(vx2, vy2); ctx.lineTo(vx6, vy6)
    ctx.stroke()
  }
} // Cube
