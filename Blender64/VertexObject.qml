import QtQuick 2.0

QtObject {
  property int num_vertices: undefined
  property int l: 80 // max. width/height in px

  // 3d vertex definitions
  property var x: undefined   //INt8Array
  property var y: undefined   //INt8Array
  property var z: undefined   //INt8Array

  property var xd: undefined  //INt8Array
  property var yd: undefined  //INt8Array
  property var zd: undefined  //INt8Array

  // projected 2d coordinates
  property var vx: undefined  //INt8Array
  property var vy: undefined  //INt8Array

  Component.onCompleted: {
    console.assert(num_vertices, 'VertexObject vertices are undefined')

    // vertices
    x  = new Int8Array(num_vertices)
    y  = new Int8Array(num_vertices)
    z  = new Int8Array(num_vertices)

    // transformed vertices
    xd = new Int8Array(num_vertices)
    yd = new Int8Array(num_vertices)
    zd = new Int8Array(num_vertices)

    // view coordinates
    vx = new Int16Array(num_vertices)
    vy = new Int16Array(num_vertices)
  }

  function seal_vertices() {
    Object.seal(self.x)
    Object.seal(self.y)
    Object.seal(self.z)

    Object.seal(self.xd)
    Object.seal(self.yd)
    Object.seal(self.zd)

    Object.seal(self.vx)
    Object.seal(self.vy)

    Object.seal(self.num_vertices)
    Object.seal(self.l)
  }
}
