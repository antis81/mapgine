import QtQuick
import QtQuick.Templates as T

T.Dial {
  id: control
//  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
//                          implicitContentWidth + leftPadding + rightPadding)
//  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
//                           implicitContentHeight + topPadding + bottomPadding)
  implicitWidth: 80
  implicitHeight: 80
  background: Image {
    source: control.pressed ? 'assets/dial_bg_pressed.svg' : 'assets/dial_bg.svg'
    x: control.width/2 - width/2
    y: control.height/2 - height/2
    width: Math.max(64, Math.min(control.width, control.height))
    height: width
    opacity: control.enabled ? 1 : 0.3
    Text { anchors.centerIn: parent; text: control.value; color: 'silver' }
  }
  handle: Image {
    source: control.pressed ? 'assets/dial_handle_pressed.svg' : 'assets/dial_handle.svg'
    sourceSize: Qt.size(16, 16)
    x: control.background.x + control.background.width/2 - width/2
    y: control.background.y + control.background.height/2 - height/2
    transform: [
      Translate {
        y: -Math.min(control.background.width, control.background.height) * 0.42 + control.handle.height / 2
      },
      Rotation {
        angle: control.angle
        origin.x: control.handle.width/2
        origin.y: control.handle.height/2
      }
    ]
  }
}
