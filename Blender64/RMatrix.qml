import QtQuick 2.0

QtObject {
  id: root

  readonly property QtObject _p: QtObject{
    property var _m: new Int8Array([
      0,0,0,
      0,0,0,
      0,0,0,
    ])
    property int a: 0
    property int b: 0
    property int c: 0
    property int d: 0
    property int e: 0
    property int f: 0
    property int g: 0
    property int h: 0
    property int i: 0
  }

  function update_matrix(a,b,c,d,e,f,g,h,i) {
    root._p._m = [a,b,c,d,e,f,g,h,i]
  }

  function values() {
    return [
        root.a,
        root.b,
        root.c,
        root.d,
        root.e,
        root.f,
        root.g,
        root.h,
        root.i
        ]
  }

  readonly property int a: _p._m[0] //root._p.a
  readonly property int b: _p._m[1] //root._p.b
  readonly property int c: _p._m[2] //root._p.c
  readonly property int d: _p._m[3] //root._p.d
  readonly property int e: _p._m[4] //root._p.e
  readonly property int f: _p._m[5] //root._p.f
  readonly property int g: _p._m[6] //root._p.g
  readonly property int h: _p._m[7] //root._p.h
  readonly property int i: _p._m[8] //root._p.i
}
