import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
  id: app_root
  visible: true
  visibility: 'Maximized'
  width: 800
  height: 600

  SplitView {
    anchors.fill: parent
    RenderView { id: render_view; SplitView.fillWidth: true }
    ToolBar { id: render_tools; SplitView.minimumWidth: 180
      ColumnLayout {
        // common properties
        Row {
          Button { id: btn_eng_run; text: qsTr('Engine Running'); checkable: true; checked: render_engine.running; onCheckedChanged: render_engine.running = checked }
          Button { id: btn_stepper; text: qsTr('Single Frame'); enabled: !render_engine.running; onClicked: render_engine.triggered() }
        }
        Button { id: btn_autorot; text: qsTr('Rotate View'); checkable: true; checked: false }

        // rotation
        Slider { id: sl_zoom; from: 0.1; to: 10.0; value: render_view.zoom; stepSize: 0.05; onValueChanged: render_view.zoom = value; }
        Row {
          CircularSlider { id: sl_rx; wrap: true; from: 0; to: render_view.rot_max; stepSize: 1; value: render_view.rx; onValueChanged: render_view.rx = Math.trunc(value) }
          CircularSlider { id: sl_ry; wrap: true; from: 0; to: render_view.rot_max; stepSize: 1; value: render_view.ry; onValueChanged: render_view.ry = Math.trunc(value) }
          CircularSlider { id: sl_rz; wrap: true; from: 0; to: render_view.rot_max; stepSize: 1; value: render_view.rz; onValueChanged: render_view.rz = Math.trunc(value) }
        }
        Label { text: "Rotation Matrix (int8)" }
        RMatrixView { id: rmatrix_view; rmatrix: render_view.rmatrix_1 }
        Label { text: "Rotation Matrix (negated)" }
        RMatrixView { id: rmatrix_view_neg; rmatrix: render_view.rmatrix_2 }
      }
    }
  }

  Timer { // updates the render_view in timer interval (50ms ~= 20FPS)
    id: render_engine
    repeat: true
    interval: 50 // ~20 FPS
    onTriggered:{
      if (btn_autorot.checked) {
        render_view.rotate()
      }
    }
    running: true
  }
}
