import QtQuick 2.14

Canvas {
  id: root
  implicitWidth: (length + horShift) * fieldSize * 2
  implicitHeight: (length*fieldSize) + hmMax + (2*padding)
  antialiasing: true
  smooth: true

  //Rectangle { anchors.fill: parent; color: "transparent"; border.color: "blue" }

  property int length: 64
  property bool fill; onFillChanged: requestPaint()
  property bool triangular: false; onTriangularChanged: requestPaint()
  property int floorlevel; onFloorlevelChanged: requestPaint()
  property real horShift; onHorShiftChanged: requestPaint()
  property real grade; onGradeChanged: requestPaint()
  property real fieldSize; onFieldSizeChanged: requestPaint()
  property int padding: 20

  readonly property var hmData: new Array(length*length); onHmDataChanged: requestPaint()
  readonly property int hmCeil: Math.max(hmData)
  readonly property int hmMax: (255*grade)+(fieldSize)
  readonly property int hmTop: padding + hmMax // heightmap top

  function draw_row(ctx, row, col) {
    const s = root.length
    const top_row_idx = s*row
    const bottom_row_idx = s*(row+1)
    let h_tl = root.hmData[top_row_idx + col] // top-left
    let h_tr = root.hmData[top_row_idx + col+1] // top-right
    let h_bl = root.hmData[bottom_row_idx + col] // bottom-left
    let h_br = root.hmData[bottom_row_idx + col+1] // bottom-right

    // helper vars
    let shift_top    = s-(row  )*root.horShift
    let shift_bottom = s-(row+1)*root.horShift
    let field_top    = root.hmTop + ((row  ) * root.fieldSize)
    let field_bottom = root.hmTop + ((row+1) * root.fieldSize)

    // top-left
    let x1 = (col+shift_top) * root.fieldSize
    let y1 = field_top - (h_tl * root.grade)
    // top-right
    let x2 = (col+1+shift_top) * root.fieldSize
    let y2 = field_top - (h_tr * root.grade)

    // bottom-left
    let x3 = (col+shift_bottom) * root.fieldSize
    let y3 = field_bottom - (h_bl * root.grade)
    // bottom-right
    let x4 = (col+1+shift_bottom) * root.fieldSize
    let y4 = field_bottom - (h_br * root.grade)

    let n = root.floorlevel
    if (h_tl<=n && h_tr<=n && h_bl<=n && h_br<=n) {
      //draw "under-water" (blue)
      ctx.strokeStyle = '#77a'
      ctx.fillStyle = '#228'
    } else {
      //mountain (green)
      ctx.strokeStyle = '#084'
      ctx.fillStyle = '#032'
    }

    // draw rectangles
    ctx.beginPath()
    ctx.moveTo(x1,y1) // left-top
    ctx.lineTo(x2,y2) // right-top
    ctx.lineTo(x4,y4) // right-bottom
    ctx.lineTo(x3,y3) // left-bottom
    ctx.closePath() // -> ctx.lineTo(x1,y1) // left-top
    ctx.stroke()
    if (root.fill) {
      ctx.fill()
    }

    // draw triangle line
    if (root.triangular) {
      ctx.moveTo(x1,y1) // left-top
      ctx.lineTo(x4,y4) // right-bottom
      ctx.stroke()
    }
  }

  onPaint: {
    let ctx = getContext("2d");
    // ctx.clearRect (0, 0, width, height);
    ctx.reset();

    const s = root.length

    // painting
    for (let t=0; t<s-1; t++) { // rows (top-down)
      for (let i=0; i<s-1; i++) { // columns (left-right)
        draw_row(ctx,t,i)
      }
    }
  }

  Component.onCompleted: {
    for(let i=0; i<hmData.length; i++) {
      hmData[i] = 0
    }

  }
}
