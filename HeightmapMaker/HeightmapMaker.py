#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
from PySide2 import QtCore as Qt, QtGui, QtQuick


class Heightmap(Qt.QObject):
    @Qt.Slot(str, str)
    def write_bin_file(self, filename, json_data):
        print(f'Saving height-field to {filename}')
        arr = json.loads(json_data)
        assert type(arr) is list
        arr = bytes(arr)
        with open(filename, 'wb') as file:
            file.write(arr)


if __name__ == '__main__':
    app = QtGui.QGuiApplication([])
    app.setOrganizationName('C64MapGine')
    app.setOrganizationDomain('c64mapgine.org')

    obj = Heightmap()
    view = QtQuick.QQuickView()
    view.engine().quit.connect(app.quit)
    view.rootContext().setContextProperty('py_heightmap', obj)

    center = app.screens()[0].geometry().center()
    view.setWidth(1400)
    view.setHeight(700)
    view.setX(center.x() - view.width()/2)
    view.setY(center.y() - view.height()/2)
    view.setResizeMode(QtQuick.QQuickView.SizeRootObjectToView)
    view.show()
    view.setSource("HeightmapMaker.qml")
    sys.exit(app.exec_())
