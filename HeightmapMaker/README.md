# HeightmapMaker

Generate binary heightmaps from any picture!
Made for C64, but can also be used for basically any "byte-based" machine!


## Installation & Start

HeightmapMaker is completely written in Python & QML. The only dependency is PySide2:

```bash
pip install PySide2
```

After that you can simply run the Script in this directory.

```bash
./HeightmapMaker.py
```

You should now see something like this:

![HeightmapMaker Example](screenshots/HeightmapMaker.png)


## How To Use It

1. open a picture file in HeightmapMaker
1. select an interesting area to export
1. export the selected area raw data (8-bit resolution).


## Further Steps

To actually see the heightmap you will need to perform additional steps:

1. Create a disk-image (e.g. d64) and place the exported file inside.
1. Use a render engine like "mapengine.prg" to visualize the map.
