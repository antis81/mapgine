import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.14
import QtGraphicalEffects 1.0

Page {
  id: root
  width: 1200
  height: 800
  background: Rectangle { color: '#abc' }
  header: MenuBar {
    background: null
    Menu {
      title: qsTr('File')
      Action { text: qsTr('Open Picture…'); onTriggered: open_height_map.visible = true; shortcut: 'ctrl+o' }
      Action { text: qsTr('Export Heights…'); onTriggered: save_height_map.visible = true; shortcut: 'ctrl+s' }
      MenuSeparator {}
      Action { text: qsTr('Quit'); onTriggered: Qt.quit(); shortcut: 'ctrl+q' }
    }
  }

  FileDialog {
    id: open_height_map
    visible: false
    onAccepted: {
      if (fileUrl) {
        height_map.unloadImage(height_map.imgPath)
        img_map.source = fileUrl
      }
    }
  }

  FileDialog {
    id: save_height_map
    visible: false
    selectExisting: false
    defaultSuffix: 'heights'
    onAccepted: {
      if (fileUrl) {
        const path = fileUrl.toString().replace(/^(file:\/{2})/,'')
        let heights = hm_view.hmData
        py_heightmap.write_bin_file(path, JSON.stringify(heights))
      }
    }
  }

  SplitView {
    anchors.fill: parent
    background: Rectangle { color: "transparent"; border { width: 1; color: '#ccc' } }
    orientation: Qt.Horizontal

    ToolBar {
      id: panel
      //SplitView.preferredWidth: 160
      SplitView.fillHeight: true
      background: null

      ColumnLayout {
        Column{ Text{text:'Map Size'} ComboBox {
            model: [8,16,32,64,128]
            currentIndex: 3 // 64x64 default
            displayText: 'Size: ' + currentValue + 'x' + currentValue
            onCurrentValueChanged: hm_view.length = currentValue
          }
        }
        Column{ Text{text:'Field Size ['+field_size_slider.value.toFixed(2)+']'} Slider{id: field_size_slider; from: 1; to: 50; value: 12} }
        Column{ Text{text:'Horizontal Shift ['+hor_shift_slider.value.toFixed(2)+']'} Slider{id: hor_shift_slider; from: -1; value: 0.5} }
        Column{ Text{text:'Grade ['+grade_slider.value.toFixed(2)+']'} Slider{id: grade_slider; from: 0.05; to: 10; stepSize: 0.05; value: 1.0} }
        Column{ Text{text:'Floorlevel ['+floorlevel_slider.value+']'} Slider{id: floorlevel_slider; from: 0; to: 256; stepSize: 1; value: 0} }

        CheckBox{ id: btn_fill; text: "Filled" }
        CheckBox{ id: btn_triangular; text: "Draw Triangles" }

        // test pattern
        //Button{ text:"Bottom-L -> Top-R"; onClicked: hf_view.hf_bl_to_tr(); }
      }
    }

    Image {
      id: img_map
      visible: false
      source: 'images/commodore-logo.png'
      width: sourceSize.width
      height: sourceSize.height
    }

    SplitView {
      orientation: Qt.Horizontal
      SplitView.fillWidth: true

      Rectangle {
        SplitView.preferredHeight: parent.height * 0.75
        SplitView.preferredWidth: parent.width * 0.50
        clip: true
        color: "#556"

        Canvas {
          id: height_map
          implicitWidth: img_map.implicitWidth // image width
          implicitHeight: img_map.implicitHeight // image height

          property var ctx: null
          property string imgPath: img_map.source

          onPaint: {
            ctx = getContext("2d")
            ctx.reset()
            ctx.drawImage(imgPath,0,0)
            ctx.stroke()
          }
          onImageLoaded: requestPaint()
          Component.onCompleted: loadImage(imgPath)

          Rectangle {
            id: selector
            width: hm_view.length; height: width
            color: "#408080ff"
            function move(x,y) {
              x = Math.min(Math.max(width/2,x), height_map.implicitWidth-width/2)
              y = Math.min(Math.max(height/2,y), height_map.implicitHeight-height/2)
              selector.x = x - (selector.width/2)
              selector.y = y - (selector.height/2)

              updateHeightmap()
            }
            function updateHeightmap() {
              var ctx = height_map.ctx
              if (!ctx) {
                // Context2D not assigned -> cannot have height data
                return;
              }

              if (!height_map.isImageLoaded(height_map.imgPath)) {
                console.error('height map image not loaded')
                return
              }

              var imgData = ctx.getImageData()
              if (!imgData) {
                //console.info('no image data -> try to create it')
                imgData = ctx.createImageData(height_map.imgPath)
              }
              if (!imgData) {
                console.error('FATAL -> no image data')
                return
              }

              const s = hm_view.length
              for (var t=0; t<s; t++) { // top-down
                const y_idx = height_map.width * (selector.y+t)
                for (var i=0; i<s; i++) { // left-right
                  const x_idx = selector.x + i
                  const arr_idx = 4 * (y_idx + x_idx)
                  const r = imgData.data[arr_idx]
                  const g = imgData.data[arr_idx+1]
                  const b = imgData.data[arr_idx+2]
                  // gray value from qGray()
                  hm_view.hmData[s*t + i] = (r * 11 + g * 16 + b * 5)/32
                }
              }

              //console.log(height_map.values)
              hm_view.requestPaint()
            }
          }

          MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onPositionChanged: {
              if (pressed && pressedButtons === Qt.LeftButton) {
                selector.move(mouseX, mouseY)
              }
            }
            onPressedButtonsChanged: {
              if (pressedButtons === Qt.LeftButton) {
                selector.move(mouseX, mouseY)
              }
            }
          }
        }
      }

      ColumnLayout {
        spacing: 0
        SplitView.fillWidth: true
        SplitView.fillHeight: true

        Rectangle {
          id: hm_header
          Layout.fillWidth: true
          implicitHeight: 50
          color: "#77a"

          Text {
            anchors.top: parent.top
            text: "Canvas: "+Math.round(hm_view.implicitWidth)+'x'+Math.round(hm_view.implicitHeight)
          }
        }

        ScrollView {
          id: hm_container
          Layout.fillWidth: true
          Layout.fillHeight: true
          clip: true
          background: Rectangle{color: "black"}
          ScrollBar.horizontal.policy: ScrollBar.AlwaysOn
          ScrollBar.vertical.policy: ScrollBar.AlwaysOn
          Component.onCompleted: {
            ScrollBar.horizontal.position = 0.5
            ScrollBar.vertical.position = 0.5
          }

          Heightmap {
            id: hm_view
            padding: 40
            triangular: btn_triangular.checked
            fill: btn_fill.checked
            fieldSize: field_size_slider.value
            floorlevel: floorlevel_slider.value
            grade: grade_slider.value
            horShift: hor_shift_slider.value
          }
        }
      }
    }
  }
}
