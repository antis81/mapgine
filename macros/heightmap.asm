!print "heightmap imported"

!ifdef NESTED_INCLUDE {
  !script "three_d.lua"

  !include "gfxhires.asm"
  !include "../memmap.asm"
}

; routines: heightmap_set_field_size
!macro HEIGHTMAP_INIT(heightmap_set_field_size){
  lda #8 ; FIXME: temporary zoom factor "stretching depth"
  sta vars.heightmap_field_width ; init field size
  jsr heightmap_set_field_size
}

; Updates the field size (in px).
;
; The resulting (16-bit) table contains the offset (origin)
; for each individual field in the map.
;
; routines:   mul_x
!macro HEIGHTMAP_SET_FIELD_WIDTH(mul_x) {
  ldy #0
  ; first value is always 0
  sty r_tables.heightmap_field_offsets_lo
  sty r_tables.heightmap_field_offsets_hi
  !ifdef HM_REVOFFSETS {
    sty r_tables.heightmap_field_revoffsets_lo + heightmap.width-1
    sty r_tables.heightmap_field_revoffsets_hi + heightmap.width-1
  }
  .loop:  ; yreg = [1..heightmap.width-1]
    iny

    ; multiply (accu*xreg -> index * field_width)
    tya
    ldx vars.heightmap_field_width
    jsr mul_x
    sta r_tables.heightmap_field_offsets_hi,y
    txa ; cannot be stored directly
    sta r_tables.heightmap_field_offsets_lo,y

    !ifdef HM_REVOFFSETS {
      ; fill reverse table
      tya
      sec
      inv_imm(heightmap.width-1)
      tax
      ; xreg = abs(heightmap.width-index)-1 -> reverse index [heightmap.width-1..1]
      lda r_tables.heightmap_field_offsets_lo,y
      sta vars.heightmap_field_revoffsets_lo,x

      lda r_tables.heightmap_field_offsets_hi,y
      sta vars.heightmap_field_revoffsets_hi,x
    }

    cpy #heightmap.width-1
    bne .loop
}

; inverts a heightmap index
;
; inversion: [heightmap.width-1..0] -> [0..heightmap.width-1]
;
; note: set carry (sec) before macro
;
!macro HEIGHTMAP_INV_IDX(idx, inverted) {
  lda #heightmap.width-1
  sbc idx
  sta inverted
}

; simple zoom "out" by dividing -> P' = P(x,y)/2
!macro HEIGHTMAP_ZOOM(screen_col, screen_row) {
  ; col' = col/2
  lda screen_col+1
  lsr
  sta screen_col+1
  lda screen_col
  ror
  sta screen_col

  ; row' = row/2
  lda screen_row
  lsr
  sta screen_row
}

; converts a (positive) view y to screen row
;
; accu: contains result
;
; out: view_x
;
!macro HEIGHTMAP_X_TO_SCREEN(view_x){
  .hCenter = GFXHIRES_WIDTH/2

  sec
  lda #.hCenter
  sbc view_x
  sta view_x
  lda #0
  sbc view_x+1
  sta view_x+1
}

; converts a (positive) view y to screen row
;
; accu: contains result (carry cleared on overflow)
;
; out: view_y
;
!macro HEIGHTMAP_Y_TO_SCREEN(view_y) {
  .vCenter = GFXHIRES_HEIGHT/2

  clc
  adc #.vCenter
  sta view_y
}

; loads accumulator with the depth value for a heightmap row in range [0..63].
;
; accu: resulting depth value
;
; in:     row_idx
; tables: y_fact
!macro HEIGHTMAP_PROJECT_ROW(row_idx, y_fact) {
  ; depth = round(d/(y_0 - y/64))
  lda row_idx ; heightmap row
  ; map row_idx to max out the range -> [0,1,2, 3, 4,…, 63]
  SHL(2)      ; index = 4 * row_idx  -> [0,4,8,12,16,…,252]
  eor #$7f    ; index = 127 - index  -> map from uint8
  tax
  lda y_fact,x
}

; Projects a vertex to screen coordinates.
;
; @param[in] heightmap_col    (uint8) basis for vertex x value
; @param[in] heightmap_row    (uint8) basis for vertex y value (depth)
; @param[in] heightmap_height (uint8) basis for vertex z value (height)
;
; @param[out] screen_row  (int16) factor 0 for multiplication
; @param[out] screen_col  (int8)  factor 0 for multiplication
; @param[out] mul_f0      (int16) factor 0 for multiplication
; @param[out] mul_f1      (int16) factor 1 for multiplication
; @param[out] mul_result  (int32) multiplication result
;
; tables:   y_fact
;
; routines: mul_int8, mul_uint8
;
!macro HEIGHTMAP_PROJECT_VERTEX(heightmap_col, heightmap_row, heightmap_height, screen_row, screen_col, mul_f0, mul_f1, mul_result, y_fact, mul_int8, mul_uint8) {
  .CENTER_COL = floor(heightmap.columns/2)
  ; .MAX_DEPTH = ThreeD_ProjectionFactorsMax(y_fact)

  ; get depth value
  HEIGHTMAP_PROJECT_ROW(heightmap_row, y_fact)
  sta mul_f0 ; depth value (temp)

  ; x' = vertex_x * depth
  sec ; reinitialize mul_f0
  lda #.CENTER_COL
  sbc heightmap_col
  sec ; restore carry
  sta mul_f1
  jsr mul_int8
  lda mul_result
  sta screen_col   ; low
  lda mul_result+1
  sta screen_col+1 ; high

  ; y' = vertex_z * vertex_z * depth + depth
  lda heightmap_height
  sta mul_f1
  clc ; reuse mul_f0 (depth factor)
  jsr mul_int8
  lda mul_result
  eor #$ff
  ;sta screen_row  ; row = z'
  ; TODO: high-byte ignored
  HEIGHTMAP_Y_TO_SCREEN(screen_row)
  HEIGHTMAP_X_TO_SCREEN(screen_col)
}

; performs linear transformations on a heightmap line
;
; A line is represented by the two 3D vertices P1(x,y,z) and P2(x,y,z)
; where x and y represent the 8-bit "field index" in the range [0..heightmap_width-1].
;
; Transformations are done in order:
; 1. "stretch" the x0/1 and y0/1 8bit line endpoints according to heightmapFieldWidth
;    -> the result may be 16-bit
; 2. TODO: perform linear transformations (rotation)
; 3. project from 3D space to screen coordinates
; 4. TODO: zoom the view
;
; out:      mul_f0, mul_f1, mul_result,
; tables:   y_fact
; routines: mul_int8, mul_uint8
!macro HEIGHTMAP_TRANSFORM_LINE(mul_f0, mul_f1, mul_result, y_fact, mul_int8, mul_uint8){
  .vertex_x0 = _heightmap_col
  .vertex_y0 = _y0
  .vertex_z0 = vars.z0

  .vertex_x1 = vars.x1
  .vertex_y1 = _y1
  .vertex_z1 = vars.z1

  sec ; carry remains set
  HEIGHTMAP_INV_IDX(_heightmap_row0, .vertex_y0)
  HEIGHTMAP_INV_IDX(_heightmap_row1, .vertex_y1)

  ; TODO: rotate line "start" vertex x0,y0,z0
  ; TODO: rotate line "end" vertex x1,y1,z1

  ; project to view
  HEIGHTMAP_PROJECT_VERTEX(.vertex_x0, .vertex_y0, .vertex_z0, _y0, _x0, mul_f0, mul_f1, mul_result, y_fact, mul_int8, mul_uint8)
  HEIGHTMAP_PROJECT_VERTEX(.vertex_x1, .vertex_y1, .vertex_z1, _y1, _x1, mul_f0, mul_f1, mul_result, y_fact, mul_int8, mul_uint8)
}

; tables:   heightmap_table_row_lo, heightmap_table_row_hi, y_fact,
; routines: heightmap_line
;
!macro HEIGHTMAP_DRAW(heightmap_table_row_lo, heightmap_table_row_hi, y_fact, heightmap_line) {
  .heightmapRowBottom = heightmap.width
  .heightmapRowTop = .heightmapRowBottom-heightmap.depth
  .ROW_ADDR = $ffff
  ; notes:
  ; - using a right-handed (x,y,z) coordinate system with z-axis facing "up"
  ; - _heightmap_col and _heightmap_row0 represent x0,y0
  ; - bottom line is drawn first, left line second

  ; walk heightmap rows [heightmapRowBottom-1..heightmapRowTop]
  ; start with row/col index at bottom-right
  ldy #.heightmapRowBottom-1
  .row_loop:
    sty _heightmap_row0 ; y0 (field bottom)
    sty _heightmap_row1 ; y1 = y0

    ; init the row pointers (bottom)
    lda heightmap_table_row_lo,y
    sta .heightmap_bl +1
    sta .heightmap_br +1 ; row(low)
    lda heightmap_table_row_hi,y
    sta .heightmap_bl +2
    sta .heightmap_br +2 ; row(high)
    !ifdef HM_VERTICALS {
      cpy #.heightmapRowTop
      beq .n0
        dey ; row index "above"
      .n0: MEM_SET_POINTER_Y(heightmap_table_row_lo, heightmap_table_row_hi, (.heightmap_tl +1))
    }

    ; walk heightmap columns [0..heightmap.width-1]
    ldx #0
    .col_loop:
      stx _heightmap_col ; x0
      .heightmap_bl:
        lda .ROW_ADDR,x ; ROW_ADDR will be modified
        sta vars.z0

      cpx #heightmap.columns
      beq .next_row
        cpx #heightmap.columns-1
        beq .n1 ; last column?
          inx
          stx vars.x1
          .heightmap_br:
            lda .ROW_ADDR,x ; ROW_ADDR will be modified
            sta vars.z1

          jsr heightmap_line ; draw field bottom line
          ldx _heightmap_col ; restore xreg
        .n1:
        !ifdef HM_VERTICALS {
          lda _heightmap_row0
          cmp #.heightmapRowTop
          beq .n2
            ; get y and z from next row
            stx vars.x1 ; x1 = x0 -> dec vars.x1
            dec _heightmap_row1
            .heightmap_tl:
              lda .ROW_ADDR,x ; ROW_ADDR will be modified
              sta vars.z1

            jsr heightmap_line ; draw field left line
            ldx _heightmap_col ; restore xreg
            inc _heightmap_row1 ; restore
          .n2:
        }

      ; next column
      inx
      jmp .col_loop

  .next_row:
    ldy _heightmap_row0 ; restore yreg
    cpy #.heightmapRowTop
    beq .n3
      dey
      jmp .row_loop
    .n3:
}
