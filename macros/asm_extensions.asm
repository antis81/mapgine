!print "asm_extensions imported"

; global bool definition
True = 1
False = 0

!macro print_deprecation_warning(dep_name, new_name) {
  !print "warning: ", dep_name, " is deprecated -> use ", new_name, " instead"
}

; %{ -- LUA code
; function _asm_is_type(arg, type) { return arg.getType() == type }
; function _asm_16bit_is_type(arg0, arg1, type) { .return arg0.getType() == type && arg1 == type }
;
; function asm_errcheck_address(arg) {
;   !if (_asm_is_type(arg, AT_IMMEDIATE)) {
;     .error "argument is not an address"
;   }
;   .return arg
; }

; returns second byte of a 16-bit value
;
; .function _asm_16bit_arg(arg) {
;     !if (arg.getType() == AT_IMMEDIATE) {
;         .return CmdArgument(arg.getType(),>arg.getValue())
;     }
;     .return CmdArgument(arg.getType(),arg.getValue()+1)
; }
; }%

; alias for axs illegal opcode (vice uses sbx)
; .pseudocommand sbx arg { axs arg }

; Load accu with value and copy to address.
; .pseudocommand mov arg0:arg1 { lda arg0; sta asm_errcheck_address(arg1) }

; simple 8bit addition
;
; arg0 (any address mode or 8bit constant)
;
; schema:
; accu += arg0
; (carry is set on overflow)
;
; .pseudocommand add arg0 { clc; adc arg0 }

; simple 8bit subtraction
;
; arg0 (any address mode or 8bit constant)
;
; schema:
; accu -= arg0
; (carry is cleared on overflow)
;
; .pseudocommand sub arg0 { sec; sbc arg0 }

; increments addr (MSB) if carry flag is set
;
; replaces: lda addr; adc #0; sta addr -> 10 cycles
; cycles min: 3
; cycles max: 8
;
!macro ADD_CARRY(addr) {
  bcc .n0 ; FIXME: no special labels in macros
    inc addr
  .n0: ; FIXME: no special labels in macros
}

; decrements addr (MSB) if carry flag is clear
;
; replaces: lda addr; sbc #0; sta addr -> 10 cycles
; cycles min: 3
; cycles max: 8
;
!macro SUB_CARRY(addr) {
  bcs .n0 ; FIXME: no special labels in macros
    dec addr
  .n0: ; FIXME: no special labels in macros
}

; @brief  inverts accu resulting in the complementary int8 value
;
; @note   make sure carry is set
;
; usage:
;   sec
;   inv
;
; cycles: 4
!macro inv() {
  eor #$ff
  adc #0
}

; same as inv, but with an argument
;
; Useful to invert a table index.
;
; arguments:
; arg0    highest possible index (with carry set) / array size (with carry clear)
;
; usage 1 (highest index):
;   sec
;   inv_ #arr_size-1
;
; usage 2 (array size):
;   clc
;   inv_ #arr_size
;
; cycles min: 4
; cycles max: 8
;
!macro inv_(addr) {
  eor #$ff
  adc addr
}
!macro inv_imm(val) {
  eor #$ff
  adc #val
}

; Compare arg0 with accu,xreg,yreg (skip if arg0 != accu,xreg,yreg).
;
; arg0: address (absolute) or value (immediate)
; skip: jump to address (relative) if not equal
;
; .pseudocommand if_a_eq(arg0 : skip) { cmp arg0; bne skip }
; .pseudocommand if_x_eq arg0 : skip { cpx arg0; bne skip }
; .pseudocommand if_y_eq arg0 : skip { cpy arg0; bne skip }

; Compare arg0 with accu,xreg,yreg (skip if arg0 == accu,xreg,yreg).
;
; arg0: address (absolute) or value (immediate)
; skip: jump to address   (r  elative) if equal
;
; .pseudocommand if_a_ne arg0 : skip { cmp arg0; beq skip }
; .pseudocommand if_x_ne arg0 : skip { cpx arg0; beq skip }
; .pseudocommand if_y_ne arg0 : skip { cpy arg0; beq skip }

; Compare arg0 with accu,xreg,yreg (skip if arg0 >= accu,xreg,yreg).
;
; arg0: address (absolute) or value (immediate)
; skip: jump to address (relative) if greater or equal
;
; .pseudocommand if_a_lt arg0 : skip { cmp arg0; bcs skip }
; .pseudocommand if_x_lt arg0 : skip { cpx arg0; bcs skip }
; .pseudocommand if_y_lt arg0 : skip { cpy arg0; bcs skip }

; Compare arg0 with accu,xreg,yreg (skip if arg0 < accu,xreg,yreg).
;
; arg0: address (absolute) or value (immediate)
; skip: jump to address (relative) if less than
;
; .pseudocommand if_a_ge arg0 : skip { cmp arg0; bcc skip }
; .pseudocommand if_x_ge arg0 : skip { cpx arg0; bcc skip }
; .pseudocommand if_y_ge arg0 : skip { cpy arg0; bcc skip }

; execute followup code if zero-flag is set (e.g. cmp #0), skip otherwise
; .pseudocommand if_zero skip { bne skip }

; execute followup code if zero-flag is not set (e.g. cmp #255), skip otherwise
; .pseudocommand if_nonzero skip { beq skip }

; execute followup code if negative flag is set
; .pseudocommand if_negative skip { bpl skip }

; execute followup code if negative flag is clear
; .pseudocommand if_positive skip { bmi skip }

; execute followup code if zero-flag is clear (e.g. cmp #255), skip otherwise
;
; @note alias for if_nonzero
;
; .pseudocommand if_true skip { if_nonzero skip }

; execute followup code if zero-flag is clear (e.g. cmp #255), skip otherwise
;
; @note alias for if_zero
;
; .pseudocommand if_false skip { if_zero skip }

; right-shifts a 16-bit value and assigns the result in resultAddr
;
; schema:
; addrResult = addr >> num
;
; note: the value at addr remains unchanged
;
; registers: accu
; cycles: 20
;
!macro SHR16(addrResult, addr, num) {
  lda addr+1 ; msb
  !rept num { lsr }
  sta addrResult+1

  lda addr
  !rept num { ror }
  sta addrResult
}

; copies a 16-bit value
;
; Bytes will be copied little endian -> target (low) & target+1 (high).
;
; cycles: 16 (12 in zeropage)
; .pseudocommand mov16 arg1:arg2 {
;   mov arg1:arg2
;   mov _asm_16bit_arg(arg1) : _asm_16bit_arg(arg2)
; }

; swaps bytes in addr0 and addr1
;
; registers: accu, stack
; Cycles: 23
;
!macro swp(addr0, addr1) {
  lda addr0
  pha
  lda addr1
  sta addr0
  pla
  sta addr1
}

; swaps bytes in addr0 and addr1
;
; registers: accu,xreg
; cycles: 16
;
!macro swpx(addr0, addr1) {
  lda addr0
  ldx addr1
  sta addr1
  stx addr0
}

; swaps bytes in addr0 and addr1
;
; Registers: accu,yreg
; cycles: 16
!macro swpy(addr0,addr1) {
  lda addr0
  ldy addr1
  sta addr1
  sty addr0
}

; swaps 16-bit values in addr0 and addr1
;
; Implies little-endian in source and target.
;
; registers: accu,xreg
; cycles: 32
;
!macro swpx16(arg0,arg1) {
  swpx(arg0  , arg1  ) ; low
  swpx(arg0+1, arg1+1) ; high
}

; swaps 16-bit values in addr0 and addr1
;
; Implies little-endian in source and target.
;
; registers: accu,yreg
; cycles: 32
;
!macro swpy16(arg0,arg1) {
  swpy(arg0  , arg1  ) ; low
  swpy(arg0+1, arg1+1) ; high
}

; decrements a 16bit value (value -= 1)
;
; @param addr  the address containing 16bit value
;
; cycles min: 10
; cycles max: 18
;
!macro DEC16(addr) {
  lda addr
  bne .n0 ; FIXME: no special labels in macros
    dec addr+1
  .n0: ; FIXME: no special labels in macros
    dec addr
}

; decrements a 16bit value (value -= 1)
;
; @param addr  the address containing 16bit value
;
; cycles min: 7
; cycles max: 14
;
!macro INC16(addr) {
  inc addr
  bne .n0 ; FIXME: no special labels in macros
    inc addr+1
  .n0: ; FIXME: no special labels in macros
}

; left-shifts accu
!macro SHL(num) {
  !rept num { asl }
}

; right-shifts accu
!macro SHR(num) {
  !rept num { asr }
}
