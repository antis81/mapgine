-- Math = {}

-- rounds to integer
function MathRound(num)
  if num >= 0 then
    return math.floor(num+.5)
  else
    return math.ceil(num-.5)
  end
end

-- sinus table
--
-- size: 256 bytes
function MathMakeSin(amp, size)
  local table = {}
  for a=0, size-1 do
    table[a+1] = MathRound(math.sin(2.0 * math.pi * a / size) * amp)
  end
  return table
end

-- cosinus table
--
-- size: <size> bytes
function MathMakeCos(amp, size)
  table = {}
  for a=0, size-1 do
    table[a+1] = MathRound(math.cos(2.0 * math.pi * a / size) * amp)
  end
  return table
end
