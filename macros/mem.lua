-- Mem = {}

function MemArrayStr(name, from, to)
  local size = to - from + 1
  return name .. ": $" ..  string.format("%04x", from) .. "-$" .. string.format("%04x", to) .. " (" .. string.format("%d", size) .. " bytes)"
end
