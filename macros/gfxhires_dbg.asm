!print "gfxhires_dbg imported"

!include "gfxhires.asm" ; -> importonce!

; out: x0, y0, x1, y1
; routines: draw_line, gfx_clear
; in: count
!macro GFXHIRES_RUN_TESTS(x0, y0, x1, y1, draw_line, gfx_clear,count) {
  ; Test: draw left-to-right
  jsr gfx_clear
  ; first line: P0(0,199) -> P1(319,100)
  mov16 #0 : x0; mov #199 : y0
  mov16 #319 : x1; mov #100 : y1
  jsr !loop+

  ; Test #2: draw right-to-left
  jsr gfx_clear
  ; first line: P0(319,199) -> P1(0,100)
  mov16 #319 : x0; mov #199 : y0
  mov16 #0 : x1; mov #100 : y1
  jsr !loop+
  jmp !macro_end+

  .loop:
    jsr draw_line
    dec y0 ; please ensure always y0 >= y1
    lda y0
    cmp #GFXHIRES_HEIGHT-count
    bne .loop
      ; done
    rts

  .macro_end:
}
