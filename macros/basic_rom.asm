!ifdef NESTED_INCLUDE {
  !include "asm_extensions.asm"
  !include "mem.asm"
}
!print "basic_rom imported"

; Sets end address of BASIC RAM.
; The address is excluded, so the last writable byte is at addr-1.
; default: $A000 -> BASIC_ROM (last byte at $9FFF)
;
!macro BASIC_SET_END(addr) {
  lda #<addr
  sta ZP37_BASIC_END
  lda #>addr
  sta ZP37_BASIC_END+1
  ; !print "*** ", (addr-1 - MEM_BASIC_RAM), " BASIC BYTES FREE ***"
}

; Swaps two BASIC float values
;
; registers: accu,xreg
;
!macro BASIC_SWAP_FLOAT(addr0, addr1) {
  !rept 5 {
    swpx(addr0+i, addr1+i)
  }
}

; reads the next parameter (must be a number)
!macro BASIC_NEXT_NUM() {
  jsr $aefd	// check on comma
  jmp $ad8a
}

; OBSOLETE: generic macro with custom function
; !macro BASIC_GET_VALUE(basic_get_value_func) { jsr basic_get_value_func; jmp $b79b }
; !macro BASIC_GET_VALUE_FUNC() {
;   ; called by BASIC_GET_VALUE
;   jsr $79
;   cmp #$2c
;   beq !ret+
;   jmp $af08
; $ret:
;   rts
; }
;
!macro BASIC_GET_VALUE() {
  ; called by BASIC_GET_VALUE
  jsr $79
  cmp #$2c
  beq !ret+
  jmp $af08
$ret:
  ;rts
  jmp $b79b
}


; Compares value in addr with FAC.
;
; Result stored in accu:
; - 0x00: value == FAC
; - 0x01: value > FAC
; - 0xff: value < FAC
;
!macro BASIC_FAC_CMP(addr) { ldy #>addr; lda #<addr; jsr $bc5b }

; Stores a 8-bit value to FAC.
!macro BASIC_UINT8_TO_FLOAT(byte, uint8_to_float_func) {
  ldy #byte
  jsr uint8_to_float_func
}
!macro BASIC_UINT8_TO_FLOAT_FUNC() { jmp $b3a2 }

; Stores FAC variable (5 bytes) at addr.
!macro BASIC_FAC_TO_VAR(addr, fac_to_var_func) {
  ldy #>addr
  lda #<addr
  jsr fac_to_var_func
}
!macro BASIC_FAC_TO_VAR_FUNC() {
  ; called by FAC_TO_VAR
  sta $49 ; low
  sty $4a ; high
  tax
  jmp $bbd0
}

; Store float value (5 bytes) at addr to FAC.
!macro BASIC_VAR_TO_FAC(addr) {
  ldy #>addr
  lda #<addr
  jsr $bba2
}

; float to int16 cast
!macro BASIC_FAC_TO_INT16() { jsr $b7f7 }

; Performs an FAC addition of two uint8 values.
;
; Schema:
;   addrResult = v0 + v1
;
!macro BASIC_ADD_FLOAT_UINT8(addrResult, v0, v1, fac_to_var_func, uint8_to_float_func) {
  BASIC_UINT8_TO_FLOAT(v0, uint8_to_float_func)
  BASIC_FAC_TO_VAR(addrResult, fac_to_var_func)

  BASIC_UINT8_TO_FLOAT(v1, uint8_to_float_func)
  ldy #>addrResult
  lda #<addrResult
  jsr $b867

  BASIC_FAC_TO_VAR(addrResult, fac_to_var_func)
}

; result = addr - FAC
; Resulting float is stored in accu(low) and y-reg(high)
;
!macro BASIC_FAC_SUB(addr) { ldy #>addr; lda #<addr; jsr $b850 }

; result = addr * FAC
; Resulting float is stored in accu(low) and y-reg(high)
;
!macro BASIC_FAC_MUL(addr) { ldy #>addr; lda #<addr; jsr $ba28 }

; result = addr / FAC
; Result is stored in accu(low) and y-reg(high)
;
!macro BASIC_FAC_DIV(addr) { ldy #>addr; lda #<addr; jsr $bb0f }
