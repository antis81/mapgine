!print "math imported"

!script "math.lua"

;multiplies two uint8 values resulting in a uint16 product
;
;@param zpTmp  a zeropage address for temporary/internal use
;@return The result will be in accu(high) and xreg(low).
;
;algorythm written by Damon Slye (copied from codebase64.org )
;
;usage:
; ; define the subroutine macro in your code
; .label zpTmp = ZP02_UNUSED
; mul_ax: MATH_MUL_AX(zpTmp)
;
; ; 6*7 = 42 -> $002c
; lda #6; ldx #7; jsr multiply
;
!macro MATH_UINT8_FASTMUL_X(zpTmp) {
  .multiplier = zpTmp
  .placeholder = 0

  cpx #0
  beq .zro
    ; xreg != 0
    dex
    stx .add_x+1
    lsr
    sta .multiplier

    ; loop until xreg==0
    lda #$00
    ldx #$08
    .loop:
      bcc +
        .add_x: adc #.placeholder ; placeholder is modified
      $:
      ror
      ror .multiplier
      dex
      bne .loop
        ldx .multiplier
        rts

  .zro:
    ; xreg == 0
    txa
    rts
}

; multiplies two uint8 values resulting in a uint16 product
;
; @param[out] zpTmp  a zeropage address for temporary/internal use
; @return The result will be in accu(high) and yreg(low).
;
; algorythm written by Damon Slye (from codebase64.org)
;
; usage:
;   ; define the subroutine macro in your code
;   .label zpTmp = ZP02_UNUSED
;   multiply: MATH_MUL_AY(zpTmp)
;
;   ; 6*7 = 42 -> $002c
;   lda #6; ldx #7; jsr multiply
!macro MATH_UINT8_FASTMUL_Y(zpTmp) {
  .multiplier = zpTmp
  .placeholder = 0

  cpy #0
  beq .zro
    ; yreg != 0
    dey
    sty .add_y+1
    lsr
    sta .multiplier

    ; loop until xreg==0
    lda #$00; ldy #$08
    .loop:
      bcc +
        .add_y: adc #.placeholder ; placeholder is modified
      $:
      ror
      ror .multiplier
      dey
      bne .loop
        ldy multiplier
        rts

  .zro:
    ; yreg == 0
    tya
    rts
}

; creates the 8bit multiplication table
;
; Table generation: I:0..511
;                   square0_lo = <((I*I)/4)
;                   square0_hi = >((I*I)/4)
!macro MATH_MAKE_MULTAB(square0_hi, square0_lo){
      ldx #0
      txa
      !byte $c9   ; CMP #immediate - skip TYA and clear carry flag
.lb1  tya
      adc #0
.ml1  sta square0_hi,x
      tay
      cmp #64
      txa
      ror
.ml9  adc #0
      sta .ml9+1
      inx
.ml0  sta square0_lo,x
      bne .lb1
      inc .ml0+2
      inc .ml1+2
      clc
      iny
      bne .lb1

      !ifdef MATH_MUL_REINIT {
        ; restore modified code
        lda #0
        sta .ml9+1            ; summand
        lda #>square0_lo
        sta .ml0+2  ; table index (lo-byte)
        lda #>square0_hi
        sta .ml1+2  ; table index (hi-byte)
      }
}

; creates the 16bit multiplication tables
; Table generation: I:0..511
;                   square0_lo = <((I*I)/4)
;                   square0_hi = >((I*I)/4)
;                   square1_lo = <(((I-255)*(I-255))/4)
;                   square1_hi = >(((I-255)*(I-255))/4)
!macro MATH_MAKE_MULTAB16(square0_hi, square0_lo, square1_hi, square1_lo) {
  MATH_MAKE_MULTAB(square0_hi, square0_lo)

  ldx #$00
  ldy #$ff
  .loop:
    lda square0_hi+1,x
    sta square1_hi+$100,x
    lda square0_hi,x
    sta square1_hi,y

    lda square0_lo+1,x
    sta square1_lo+$100,x
    lda square0_lo,x
    sta square1_lo,y

    dey
    inx
  bne .loop
}

; Description: uint8 multiplication with uint16 result.
;
; Input: uint8 value in T1
;        uint8 value in T2
;        Carry=0: Re-use T1 from previous multiplication (faster)
;        Carry=1: Set T1 (slower)
;
; Output: uint16 value in PRODUCT
;
; Clobbered: PRODUCT, X, A, C
;
; Allocation setup: T1,T2 and PRODUCT preferably on Zero-page.
;                   square0_lo, square0_hi, square1_lo, square1_hi must be
;                   page aligned. Each table are 512 bytes. Total 2kb.
;
; Table generation: I:0..511
;                   square0_lo = <((I*I)/4)
;                   square0_hi = >((I*I)/4)
;                   square1_lo = <(((I-255)*(I-255))/4)
;                   square1_hi = >(((I-255)*(I-255))/4)
; in:     T1, T2,
; out:    PRODUCT,
; tables: square0_hi, square0_lo, square1_hi, square1_lo
;
!macro MATH_UINT8_MUL_X(T1, T2, PRODUCT, square0_hi, square0_lo, square1_hi, square1_lo) {
  bcc .n0 ; FIXME: no special labels in macros
    lda T1
    sta .sm1 +1
    sta .sm3 +1
    eor #$ff
    sta .sm2 +1
    sta .sm4 +1
  .n0: ; FIXME: no special labels in macros

  ldx T2
  sec
  .sm1: lda square0_lo,x
  .sm2: sbc square1_lo,x
  sta PRODUCT+0
  .sm3: lda square0_hi,x
  .sm4: sbc square1_hi,x
  sta PRODUCT+1

  rts
}

; Description: int8 multiplication with int16 result
;
; Input: 8-bit signed value in T1
;        8-bit signed value in T2
;        Carry=0: Re-use T1 from previous multiplication (faster)
;        Carry=1: Set T1 (slower)
;
; Output: 16-bit signed value in PRODUCT
;
; Clobbered: PRODUCT, X, A, C
;
; in:       T1, T2
; out:      PRODUCT
; routines: mul_x
;
!macro MATH_INT8_MUL_X(T1,T2, PRODUCT, mul_x){
  jsr mul_x

  ; Apply sign (See C=Hacking16 for details).
  lda T1
  bpl .n0 ; FIXME: no special labels in macros
    sec
    lda PRODUCT+1
    sbc T2
    sta PRODUCT+1
  .n0: ; FIXME: no special labels in macros
  lda T2
  bpl .n1
    sec
    lda PRODUCT+1
    sbc T1
    sta PRODUCT+1
  .n1:

  rts
}

; Description: uint16 multiplication with uint32 result
;
; Input: uint16 value in T1
;        uint16 value in T2
;        Carry=0: Re-use T1 from previous multiplication (faster)
;        Carry=1: Set T1 (slower)
;
; Output: uint32 value in PRODUCT
;
; Clobbered: PRODUCT, X, A, C
;
; Allocation setup: T1,T2 and PRODUCT preferably on Zero-page.
;                   square0_lo, square0_hi, square1_lo, square1_hi must be
;                   page aligned. Each table are 512 bytes. Total 2kb.
;
; Table generation: I:0..511
;                   square0_lo = <((I*I)/4)
;                   square0_hi = >((I*I)/4)
;                   square1_lo = <(((I-255)*(I-255))/4)
;                   square1_hi = >(((I-255)*(I-255))/4)
;
; in:       T1, T2,
; out:      PRODUCT,
; tables:   square0_hi, square0_lo, square1_hi, square1_lo
;
!macro MATH_UINT16_MUL_X(T1, T2, PRODUCT, square0_hi, square0_lo, square1_hi, square1_lo) {
  ; <T1 * <T2 = AAaa
  ; <T1 * >T2 = BBbb
  ; >T1 * <T2 = CCcc
  ; >T1 * >T2 = DDdd
  ;
  ;       AAaa
  ;     BBbb
  ;     CCcc
  ; + DDdd
  ; ----------
  ;   PRODUCT!

  ; Setup T1 if changed
  bcc .n0 ; FIXME: no special labels in macros
    lda T1+0
    sta .sm1a+1
    sta .sm3a+1
    sta .sm5a+1
    sta .sm7a+1
    eor #$ff
    sta .sm2a+1
    sta .sm4a+1
    sta .sm6a+1
    sta .sm8a+1
    lda T1+1
    sta .sm1b+1
    sta .sm3b+1
    sta .sm5b+1
    sta .sm7b+1
    eor #$ff
    sta .sm2b+1
    sta .sm4b+1
    sta .sm6b+1
    sta .sm8b+1
  .n0: ; FIXME: no special labels in macros

  ; Perform <T1 * <T2 = AAaa
  ldx T2+0
  sec
  .sm1a: lda square0_lo,x
  .sm2a: sbc square1_lo,x
  sta PRODUCT+0
  .sm3a: lda square0_hi,x
  .sm4a: sbc square1_hi,x
  sta ._AA+1

  ; Perform >T1_hi * <T2 = CCcc
  sec
  .sm1b: lda square0_lo,x
  .sm2b: sbc square1_lo,x
  sta ._cc+1
  .sm3b: lda square0_hi,x
  .sm4b: sbc square1_hi,x
  sta ._CC+1

  ; Perform <T1 * >T2 = BBbb
  ldx T2+1
  sec
  .sm5a: lda square0_lo,x
  .sm6a: sbc square1_lo,x
  sta ._bb+1
  .sm7a: lda square0_hi,x
  .sm8a: sbc square1_hi,x
  sta ._BB+1

  ; Perform >T1 * >T2 = DDdd
  sec
  .sm5b: lda square0_lo,x
  .sm6b: sbc square1_lo,x
  sta ._dd+1
  .sm7b: lda square0_hi,x
  .sm8b: sbc square1_hi,x
  sta PRODUCT+3

  ; Add the separate multiplications together
  clc
  ._AA: lda #0
  ._bb: adc #0
  sta PRODUCT+1
  ._BB: lda #0
  ._CC: adc #0
  sta PRODUCT+2
  bcc .n1
    inc PRODUCT+3
    clc
  .n1:
  ._cc: lda #0
  adc PRODUCT+1
  sta PRODUCT+1
  ._dd: lda #0
  adc PRODUCT+2
  sta PRODUCT+2
  bcc .n2
    inc PRODUCT+3
  .n2:

  rts
}

; Description: int16 multiplication with int32 result.
;
; Input: 16-bit signed value in T1
;        16-bit signed value in T2
;        Carry=0: Re-use T1 from previous multiplication (faster)
;        Carry=1: Set T1 (slower)
;
; Output: 32-bit signed value in PRODUCT
;
; Clobbered: PRODUCT, X, A, C
;
; in:         T1, T2
; out:        PRODUCT
; routines:   mul_uint16
!macro MATH_INT16_MUL_X(T1, T2, PRODUCT, mul_uint16) {
  jsr mul_uint16

  ; Apply sign (See C=Hacking16 for details).
  lda T1+1
  bpl +
    sec
    lda PRODUCT+2
    sbc T2+0
    sta PRODUCT+2
    lda PRODUCT+3
    sbc T2+1
    sta PRODUCT+3
  $:
  lda T2+1
  bpl +
    sec
    lda PRODUCT+2
    sbc T1+0
    sta PRODUCT+2
    lda PRODUCT+3
    sbc T1+1
    sta PRODUCT+3
  $:

  rts
}
