!enum VIC {
  BORDER_COLOR     = $d020
  BACKGROUND_COLOR = $d021
}

; Sets VIC to hires mode in $d011.
!macro VIC_SELECT_MODE_HIRES() {
  lda $d011
  and #$80 ; %0001 0000
  ora #$3b ; %0011 1011
  sta $d011
}

; Sets VIC to text mode.
!macro VIC_SELECT_MODE_TEXT() {
  lda #$1b
  sta $d011
}

; Sets the VIC bank in $dd00.
;
; accu: holds the bitmask
;
!macro VIC_SET_BANK() { sta $dd00 }

; Sets the VIC mode in $d016 (correlates to $d011).
;
; accu: holds the bitmask
;
!macro VIC_SET_MODE() { sta $d016 }

; Selects VIC video RAM location.
;
; accu: holds the bitmask
;
!macro VIC_SET_VRAM() { sta $d018 }
