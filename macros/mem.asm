!print "mem imported"

!script  "mem.lua"

!ifdef NESTED_INCLUDE {
  !include "asm_extensions.asm"
}

!enum MEM {
  ACCU
  XREG
  YREG

  PAGE_SIZE = $0100  ; size of memory page (256 bytes)

  BASIC_RAM = $0801  ; start of BASIC RAM
  BASIC_ROM = $A000  ; default address of basic rom
}

; zeropage address constants (uint8)
ZP01_PROCESSOR_PORT = $01 ; processor port
ZP02_UNUSED         = $02 ; unused
ZP03_BASIC          = $03 ; float to int address $03(low) + $04(high)
ZP05_BASIC          = $03 ; int to float address $04(low) + $05(high)
ZP14_BASIC          = $14 ; address to BASIC int $14(low) + $15(high)
ZP26_FAC            = $26 ; FAC mul./div. result (5 bytes -> $26-$2a)
ZP37_BASIC          = $37 ; basic ram end address $37(low) + $38(high)
ZP61_FAC            = $61 ; FAC value #1 (6 bytes -> $61-$66)
ZP67_FAC            = $67 ; 8bit pointer for polynomial processing
ZP68_FAC            = $68 ; FAC overflow / round
ZP69_FAC            = $69 ; FAC value #2 (6 bytes -> $69-$6e)
ZP6F_FAC            = $6f ; FAC result when comparing two float values
ZP70_FAC            = $70 ; FAC #2: round
ZP71_FAC            = $71 ; pointer for polynomial processing $71(low) + $72(high)
ZP9E_TAPE           = $9e ; checksum for tape bus
ZP9F_TAPE           = $9f ; error correction for tape bus
ZPF7_RS232          = $f7 ; pointer to rs-232 in buffer $f7(low) + $f8(high)
ZPF9_RS232          = $f9 ; pointer to rs-232 out buffer $f9(low) + $fa(high)
ZPFB_KERNAL         = $fb ; free (used by kernal depending on operation)
ZPFC_KERNAL         = $fc ; free (used by kernal depending on operation)
ZPFD_KERNAL         = $fd ; free (used by kernal depending on operation)
ZPFE_KERNAL         = $fe ; free (used by kernal depending on operation)
ZPFF_BASIC          = $ff ; BASIC "cache" byte

; kernal addresses
!enum KERNAL {
  ; NAME        | JSR     | JUMPS TO          | DESCRIPTION
  ACPTR_FFA5    = $FFA5  ; $EE13              Input byte from serial port
  CHKIN_FFC6    = $FFC6  ; ($031E) → $F20E    Open channel for input
  CHKOUT_FFC9   = $FFC9  ; ($0320) → $F250    Open a channel for output
  CHRIN_FFCF    = $FFCF  ; ($0324) → $F157    Get a character from the input channel
  CHROUT_FFD2   = $FFD2  ; ($0326) → $F1CA    Output a character
  CINT_FF81     = $FF81  ; $FF5B              Transmit a byte over the serial bus
  CIOUT_FFA8    = $FFA8  ; $EDDD              Initialize the screen editor and VIC-II Chip
  CLALL_FFE7    = $FFE7  ; ($032C) → $F32F    Close all open files
  CLOSE_FFC3    = $FFC3  ; ($031C) → $F291    Close a logical file
  CLRCHN_FFCC   = $FFCC  ; ($0322) → $F333    Clear all I/O channels
  GETIN_FFE4    = $FFE4  ; ($032A) → $F13E    Get a character
  IOBASE_FFF3   = $FFF3  ; $E500              Define I/O memory page
  IOINIT_FF84   = $FF84  ; $FDA3              Initialize I/O devices
  LISTEN_FFB1   = $FFB1  ; $ED0C              Command a device on the serial bus to listen
  LOAD_FFD5     = $FFD5  ; $F49E              Load RAM from device
  MEMBOT_FF9C   = $FF9C  ; $FE34              Set bottom of memory
  MEMTOP_FF99   = $FF99  ; $FE25              Set the top of RAM
  OPEN_FFC0     = $FFC0  ; ($031A) → $F34A    Open a logical file
  PLOT_FFF0     = $FFF0  ; $E50A              Set or retrieve cursor location
  RAMTAS_FF87   = $FF87  ; $FD50              Perform RAM test
  RDTIM_FFDE    = $FFDE  ; $F6DD              Read system clock
  READST_FFB7   = $FFB7  ; $FE07              Read status word
  RESTOR_FF8A   = $FF8A  ; $FD15              Set the top of RAM
  SAVE_FFD8     = $FFD8  ; $F5DD              Save memory to a device
  SCNKEY_FF9F   = $FF9F  ; $EA87              Scan the keyboard
  SCREEN_FFED   = $FFED  ; $E505              Return screen format
  SECOND_FF93   = $FF93  ; $EDB9              Send secondary address for LISTEN
  SETLFS_FFBA   = $FFBA  ; $FE00              Set up a logical file
  SETMSG_FF90   = $FF90  ; $FE18              Set system message output
  SETNAM_FFBD   = $FFBD  ; $FDF9              Set up file name
  SETTIM_FFDB   = $FFDB  ; $F6E4              Set the system clock
  SETTMO_FFA2   = $FFA2  ; $FE21              Set IEEE bus card timeout flag
  STOP_FFE1     = $FFE1  ; ($0328) → $F6ED    Check if STOP key is pressed
  TALK_FFB4     = $FFB4  ; $ED09              Command a device on the serial bus to talk
  TKSA_FF96     = $FF96  ; $EDC7              Send a secondary address to a device commanded to talk
  UDTIM_FFEA    = $FFEA  ; $F69B              Update the system clock
  UNLSN_FFAE    = $FFAE  ; $EDFE              Send an UNLISTEN command
  UNTLK_FFAB    = $FFAB  ; $EDEF              Send an UNTALK command
  VECTOR_FF8D   = $FF8D  ; $FD1A              Manage RAM vectors
}

nullptr = $0000

; Adds a "jump line" callable from BASIC
; - addr_here     address of this JMP line (-> SYS 12345)
; - addr_jmp      actual target address to JMP to
; - description   help text
;
!macro MEM_SYS_JMP(addrJmp, description) {
  !print "sys", str(*), "             -> ", description
  jmp addrJmp
}

!macro MEM_FLOAT() { !fill 5, 0 }
!macro MEM_INT8()  { !fill 1, 0 }
!macro MEM_INT16() { !fill 2, 0 }
!macro MEM_INT32() { !fill 4, 0 }
!macro MEM_ARRAY(size) { !fill size, 0 }

; fills the memory page from a start byte [0..255] with zeros
!macro MEM_FILL_PAGE(start_idx) {
  !fill (256-start_idx), 0
}

; adds a uint8 value to value in addr and assigns the sum to result.
!macro MEM_ADD(result, addr0, addr1) {
  lda addr0
  clc
  adc addr1
  sta result
}

; @brief Implicit subtraction of two uint8 values
; @param addr0  address with first value
; @param addr1  address with second value
; @param addrResult   address of result
;
; Schematic:
;   result = v0 - v1
;
; cycles min: 11
; cycles max: 14
;
!macro MEM_SUBTRACT(result, addr0, addr1) {
  lda addr0
  sec
  sbc addr1
  sta result
}

; @brief Implicit subtraction of two uint8 values
; @param addr1  address with second value
; @param addrResult   address of result
;
; xreg: holds first value
;
; schema:
;   result = xreg - v1
;
; cycles min: 10
; cycles max: 12
;
!macro MEM_SUBTRACT_FROM_X(result, addr1) {
  txa
  sec
  sbc addr1
  sta result
}

; @brief Implicit subtraction of two uint8 values
; @param addr1  address with second value
; @param addrResult   address of result
;
; yreg: holds first value
;
; schema:
;   result = yreg - v1
;
; cycles min: 10
; cycles max: 12
;
!macro MEM_SUBTRACT_FROM_Y(result, addr1) {
  tya
  sec
  sbc addr1
  sta result
}

; @brief Implicit subtraction of two uint8 values
; @param addrV0       v0 address
; @param addrV1       v1 address
; @param addrResult   address of result
;
; Schematic:
;   result = v0 - v1
;
!macro MEM_SUBTRACT_UINT16(addrResult, addrV0, addrV1) {
  ; low byte
  lda addrV0
  sec
  sbc addrV1
  sta addrResult
  ; high byte
  lda addrV0+1
  sbc addrV1+1
  sta addrResult+1
}

; Fills a number of memory pages with a fill-byte.
;
; The start address is expected to be aligned (low-byte == 0).
;
; Example:
;   lda #64 ; fill-byte
;   FILL_PAGES($c000, 4) ; 4*256=1024 bytes
;
!macro MEM_FILL_PAGES(addr, numPages) {
  ldx #>addr
.fill:
  stx .fill_page +2 ; modify the page address (start at addr)
  ldy #0 ; addr should always be aligned (low-byte == 0)
.fill_page: ; fill one memory page
  sta addr,y
  iny
  bne .fill_page

  ; next page
  inx
  cpx #>addr + (MEM.PAGE_SIZE * numPages)	; check MSB
  bne .fill ; inc page adress ($c0xx to $c3xx)
}

; Fills a number of memory pages with a fill-byte.
;
; The start address is expected to be aligned (low-byte == 0).
;
; Example:
;   lda #64 ; fill-byte
;   FILL_PAGES($c000, 4) ; 4*256=1024 bytes
;
!macro MEM_FILL_PAGES_FAST(addr, numPages) {
  ldy #0 ; addr should always be aligned (low-byte == 0)
.fill_pages: ; fill one memory page
  !rept numPages { sta addr + (i * MEM.PAGE_SIZE),y }
  iny
  bne .fill_pages
}

; modifies the index pointer in addr
;
; yreg: 8-bit index value (0-255)
;
; table_lo: table containing the low-bytes
; table_hi: table containing the high-bytes
; addr: address containing the pointer
;
!macro MEM_SET_POINTER_Y(table_lo, table_hi, addr) {
  ; modify heightmap row index address
  lda table_lo,y
  sta addr
  lda table_hi,y
  sta addr+1
}
