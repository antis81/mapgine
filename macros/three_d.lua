require "math"

-- three_d_projection_d  = 280.0
-- three_d_projection_y0 = 5.0
-- three_d_projection_factors = List(256)

-- calculates the projection factor for a given y (depth)
function ThreeD_ProjectionValue(y, d, y_0)
  -- d   = @three_d_projection_d
  -- y_0 = @three_d_projection_y0
  return MathRound(d/(y_0-y/64.0))
end

-- minimum factor at factors[128]
function ThreeD_ProjectionFactorsMin(factors)
  local y = math.abs(-128) -- int8
  return factors[y+1]
end

-- maximum factor at factors[127]
function ThreeD_ProjectionFactorsMax(factors)
  local y = 127 -- int8 (highest value)
  return factors[y+1]
end

-- center factor at factors[0]
--
-- @note   As we have 256 values ([-128..-1] and [0..127])
--         the actual center is between the value at -1 and 0.
--
function ThreeD_ProjectionFactorsCenter(factors)
  local y = 0
  return factors[y+1]
end

-- Creates a lookup table for central projection factors
-- size: 256 bytes
-- format: int8
function ThreeD_MakeProjectionFactors(d, y_0)
  local factors = {}
  local minval = ThreeD_ProjectionValue(-128, d, y_0) -- int8 min

  -- make factor table range: [0..maxval-minval]
  for i=0,255 do
    -- make things signed
    local y = (i > 127) and (i-256) or i
    local q = ThreeD_ProjectionValue(y, d, y_0)
    q = q - minval -- normalize value range [0..maxval-minval]
    q = math.min(math.max(q, -127), 127) -- clamp value range to int8 (values should always be positive)
    factors[i+1] = q
  end

  return factors
end
