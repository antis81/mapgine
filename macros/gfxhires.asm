!ifdef NESTED_INCLUDE {
  !include "asm_extensions.asm"
  !include "mem.asm"
}

; color constants
BLACK       = 00 ;$00
WHITE       = 01 ;$01
RED         = 02 ;$02
CYAN        = 03 ;$03
VIOLETT     = 04 ;$04
GREEN       = 05 ;$05
BLUE        = 06 ;$06
YELLOW      = 07 ;$07
ORANGE      = 08 ;$08
BROWN       = 09 ;$09
LIGHT_RED   = 10 ;$0a
DARK_GRAY   = 11 ;$0b
GRAY        = 12 ;$0c
LIGHT_GREEN = 13 ;$0d
LIGHT_BLUE  = 14 ;$0e
LIGHT_GRAY  = 15 ;$0f

GFXHIRES_WIDTH  = 320 ; $140
GFXHIRES_HEIGHT = 200 ; $C8
GFXHIRES_TABLE_MM_SIZE = 2*GFXHIRES_WIDTH ; 640 bytes (2.5 pages)

!enum DrawMode {
  Clear
  Set
  ThreeD
}

; --- TABLES BEGIN ---

; 8-bit mask table (32*8 = 256 bytes)
!macro GFXHIRES_TABLE_BITS() {
  !rept 32 { !byte $80, $40, $20, $10, $08, $04, $02, $01 }
}

; hires screen-row low table (25*8 = 200 bytes)
!macro GFXHIRES_TABLE_SCREENROW_LOW() {
  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
  !fill 8, [ i -> $40+i ]
  !fill 8, [ i -> $80+i ]
  !fill 8, [ i -> $c0+i ]

  !fill 8, [ i -> i ]
}

; hires screen-row high table (25*8 = 200 bytes)
; addr: the graphics ram address
;
!macro GFXHIRES_TABLE_SCREENROW_HIGH(addr) {
  .bank0 = >addr
  .bank1 = >(addr+$1000)

  !fill 8, .bank0
  !fill 8, (.bank0+$1)
  !fill 8, (.bank0+$2)
  !fill 8, (.bank0+$3)

  !fill 8, (.bank0+$5)
  !fill 8, (.bank0+$6)
  !fill 8, (.bank0+$7)
  !fill 8, (.bank0+$8)

  !fill 8, (.bank0+$a)
  !fill 8, (.bank0+$b)
  !fill 8, (.bank0+$c)
  !fill 8, (.bank0+$d)

  !fill 8, (.bank0+$f)
  !fill 8, (.bank1)
  !fill 8, (.bank1+$1)
  !fill 8, (.bank1+$2)

  !fill 8, (.bank1+$4)
  !fill 8, (.bank1+$5)
  !fill 8, (.bank1+$6)
  !fill 8, (.bank1+$7)

  !fill 8, (.bank1+$9)
  !fill 8, (.bank1+$a)
  !fill 8, (.bank1+$b)
  !fill 8, (.bank1+$c)

  !fill 8, (.bank1+$e)
}

; -- TABLES END

; fills color ram with byte from accu
;
; registers:
; - accu: contains the colors byte
;
; in:
; - cram    start address of color ram
;
!macro GFXHIRES_SET_COLORS(cram) {
  MEM_FILL_PAGES_FAST(cram, 4)
}

; Fills 8000 bytes ($1f40) of memory.
;
; Modifies: y-reg
; Usage example:
;   lda #00 ; fill-byte
;   GFXHIRES_CLEAR()
;
!macro GFXHIRES_CLEAR(addr_gfx_ram) {
  MEM_FILL_PAGES_FAST(addr_gfx_ram, 31) ; 31*256 = 7936 bytes
  ; ldy #0 -> implied by FILL_PAGES
  .loop: ; fill remaining 64 bytes
    sta addr_gfx_ram+$1f00,y
    iny
    cpy #64
    bne .loop
}

; Clears "hidden point" MinMax table (DrawMode3d).
;
; @param table_mm   start address of MinMax table
; @param zp_max  any zeropage(!) address; must be initialized with a "max" value (see below)
;
; Fills 640 bytes (320*2; 2.5 memory pages) with a min/max byte pairs ($ff/$00 or $ff/$c8) starting from table_mm.
; The pattern consists of a byte pair where the first byte represents the "min" value ($ff),
; second byte represents the current "max" value for one highres screen column.
;
; The zp_max 8-bit value changes drawing behaviour:
;
; - max=200 (GFXHIRES_HEIGHT)
;   The underside of an object will not be drawn.
;   Useful only for objects representing an absolute "floor" (e.g. landscape).
;   Means nothing is visible underneath the first (bottom) line of an object.
;
; - max=0
;   Also draws the underside of an object.
;   In general useful for anything "above floor".
;
; TODO: Currently 3 pages (768 bytes) are filled.
;
!macro GFXHIRES_CLEAR_TABLE_MM(table_mm, zp_max) {
  ldx #>table_mm
  .loop1:
    ; fill one memory page
    stx .fill2+4 ; modify the page address (starting at table_mm)
    stx .fill3+4 ; modify the page address (starting at table_mm)
    ldy #<table_mm
  .fill2:
    lda #255
    sta table_mm,y ; min
    iny
  .fill3:
    lda zp_max
    sta table_mm,y ; max
    iny
    bne .fill2

    ; switch to next page
    inx
    cpx #>(table_mm + GFXHIRES_TABLE_MM_SIZE) ; fill 512 bytes (from table_mm to table_mm+$01ff)
    bne .loop1

  ; fill remaining 128 bytes (from table_mm+$0200 - table_mm+$27f)
  .last_page = table_mm+$200

  ldy #0 ; -> implied by above loop
  .loop2:
    lda #255
    sta .last_page,y
    iny
    lda zp_max
    sta .last_page,y
    iny
    cpy #<GFXHIRES_TABLE_MM_SIZE ; 128
    bne .loop2
}

; Begins a draw context.
;
; Blocks IRQ during the draw context.
; Requires GFX_END_DRAW_CONTEXT() when finished.
;
!macro GFXHIRES_BEGIN_DRAW_CONTEXT(addr_gfx_ram) {
  sei
  !if (addr_gfx_ram == $e000) {
    lda #$34
    sta ZP01_PROCESSOR_PORT ; switch to HIRAM (gfx writable)
  }
}

; Ends a draw context.
;
; Allows IRQ after GFX_BEGIN_DRAW_CONTEXT().
;
!macro GFXHIRES_END_DRAW_CONTEXT(addr_gfx_ram) {
  !if (addr_gfx_ram == $e000) {
    lda #$37
    sta ZP01_PROCESSOR_PORT ; switch to LORAM (gfx readonly)
  }
  cli
}

; Sets the 16-bit pointer to a screen byte.
;
; index-registers contain the (uint8) coordinates:
; yreg: x0(low)
; xreg: y0
;
; note: index-registers are considered "const" (values will not be changed by macro)
;
; in:  x0, table_screenrows_low, table_screenrows_high
; out: ptr_screen_byte
!macro GFXHIRES_SET_PTR_SCREEN_BYTE_XY(x0, table_screenrows_low, table_screenrows_high, ptr_screen_byte) {
  ; screen-byte = row*width + column
  ;            -> y0*320    + floor(x0/8)*8

  ; pointer(low) = column(low) + row(low)
  tya ; x0(low)
  and #%11111000 ; accu = floor(x0/8)*8 -> round column to nearest byte
  clc
  adc table_screenrows_low,x ; accu += row (y0*320)
  sta ptr_screen_byte
  ; pointer(high) = row(high) + column(high)
  lda table_screenrows_high,x
  adc x0+1
  sta ptr_screen_byte+1
}

; Sets a highres pixel at the given coordinates
!macro GFXHIRES_PSET(x0, y0, draw_mode, ptr_screen_byte, px_visible, ptr_mm_idx, table_mm, table_screenrows_low, table_screenrows_high, table_screenbits) {
  ; check screen bounds
  ldx y0 ; xreg remains unchanged until end-of-macro

  ; check screen coords
  cpx #GFXHIRES_HEIGHT
  bcs .out
    ; y is in-screen
    ldy x0 ; x(low)
    lda x0+1 ; x(high)
    beq + ; x < 256
    cmp #$02
    bcs .out  ; x < 512?
      cpy #64
      bcs .out ; out-of-screen (x >= 320)
        ; ok -> px in 320x200 screen area

  $: ; ok
    lda draw_mode
    cmp #DrawMode.ThreeD
    bne .plot
      ; Check if pixel is hidden?
      ; ptr_mm_idx = ptr_mm_idx*2 + >table_mm
      lda x0
      sta ptr_mm_idx ; low
      lda x0+1
      asl ptr_mm_idx; rol
      clc
      adc #>table_mm
      sta ptr_mm_idx+1 ; high
      ; init hidden flag
      ldy #0; sty px_visible
      txa ; accu = y0
      ; compare min
      cmp (ptr_mm_idx),y
      bcs +
        ; y0 < min (above)
        sta (ptr_mm_idx),y
        inc px_visible ; -> true(1)
      $:
        ; txa; ; -> (accu still contains y0)
        ; compare max
        iny
        cmp (ptr_mm_idx),y
        bcc +
          beq .out
          ; y0 > max (below)
          sta (ptr_mm_idx),y
          inc px_visible ; -> true(1,2)
      $:
        ; px_visible ? .plot : .out
        lda px_visible
        beq .out
          ; ok -> draw px

      ldy x0 ; restore x(low) for drawing

  .plot: ; plot at screen coordinates
    ; cycles: ~40/px
    GFXHIRES_SET_PTR_SCREEN_BYTE_XY(x0, table_screenrows_low, table_screenrows_high, ptr_screen_byte)

    ; screen_byte |= pixel_mask
    lda table_screenbits,y  ; get pixel mask from x(low)
    ldy #0
    ora (ptr_screen_byte),y
    sta (ptr_screen_byte),y ; set pixel

  .out:
}

; checks line coordinates vs screen bounds (320x200)
;
; @note: Currently a line is only drawn (rts) if all of x0,y0,x1,y1 are inside screen bounds.
;        (-> TODO: clip line if partly visible)
;
; @param[in] x0
; @param[in] x1
!macro _GFXHIRES_LINE_SCREENBOUNDS(x0,x1){
  cpx #GFXHIRES_HEIGHT
  bcs .out_of_range
    ; y0 < 200

  ; note: y0 > y1 is implied (line always drawn bottom-to-top)
  ; -> no need to check y1 (always in range [0..y0])

  ; check x0 (16bit)
  lda x0+1 ; x0 (high)
  beq .ok ; x < 256
  cmp #$02
  bcs .out_of_range
    ; x0 < 512
    lda x0
    cmp #$40
    bcs .out_of_range
      ; x0 < 320

  ; check x1 (16bit)
  lda x1+1 ; x1 (high)
  beq .ok ; x1 < 256
  cmp #$02
  bcs .out_of_range
    ; x1 < 512
    lda x1
    cmp #$40
    bcs .out_of_range
      ; x1 in range (< 320)

  jmp .ok  ; fallthrough
  .out_of_range:
    ; line (partly) out-of-screen
    ; TODO: clip line at screen bounds instead of rts
    rts
.ok: ; all x0,x1,y0,y1 are in screen range
}

!macro _GFXHIRES_LINE_ADVANCE(adv_h,iy,x0) {
  ; adv_h > 0 ? !advance_x+ : !advance_y+
  lda adv_h
  beq .no_adv_h
    bpl .inc_x ; adv_h >= 0 ? inc x0 : dec x0
      DEC16(x0) ; x0 += 1
      jmp .done
    .inc_x: INC16(x0) ; x0 -= 1
  .no_adv_h:
    ; drawing left-to-right
    lda iy
    beq .done
      dex ; y0 -= 1
  .done:
}

; clips a pixel to hires screen (320x200)
;
; preconditions:
; xreg contains y0
;
; modifies: accu, yreg
;
; in:       x0
; branches: out_of_range
!macro _GFXHIRES_PCLIP_ROW(x0, out_of_range){
  cpx #GFXHIRES_HEIGHT
  bcs out_of_range
    ; y < 200
    ldy x0 ; yreg represents x0(low)
    lda x0+1 ; x(high)
    beq .ok ; x < 256
    cmp #$02
    bcs out_of_range
      ; x < 512
      cpy #$40
      bcs out_of_range
        ; x < 320
.ok
}

; internal optimized version of PSET for drawing lines
;
; preconditions: xreg = y0
;
; cycles: ~40
; modfies: accu, yreg
;
!macro _GFXHIRES_LINE_PSET(x0, draw_mode, px_visible, ptr_table_idx, table_mm, table_screenrows_low, table_screenrows_high, table_screenbits) {
  !ifdef CLIP_PX {
    _GFXHIRES_PCLIP_ROW(x0, .out)
  } !else {
    ldy x0
  }

  lda draw_mode
  cmp #DrawMode.ThreeD
  bne .plot
    ; Check if pixel is hidden?
    ; ptr_mm_idx = ptr_mm_idx*2 + >table_mm
    sty ptr_table_idx ; x0 (low)
    lda x0+1
    asl ptr_table_idx
    rol
    clc
    adc #>table_mm
    sta ptr_table_idx+1 ; high
    ; init hidden flag
    ldy #0
    sty px_visible
    ; compare min/max
    txa
    cmp (ptr_table_idx),y
    bcs .n0 ; FIXME: no special labels in macros
      ; y0 < min (above)
      sta (ptr_table_idx),y
      inc px_visible ; -> true(1)
    .n0: ; FIXME: no special labels in macros
      txa
      iny
      cmp (ptr_table_idx),y
      bcc .n1
        beq .out
        ; y0 > max (below)
        sta (ptr_table_idx),y
        inc px_visible ; -> true(1,2)
    .n1:
      ; px_visible ? .plot : .out
      lda px_visible
      beq .out
        ; ok -> draw px

    ldy x0 ; restore x0(low) for drawing

.plot: ; plot at screen coordinates
  GFXHIRES_SET_PTR_SCREEN_BYTE_XY(x0, table_screenrows_low, table_screenrows_high, ptr_table_idx)

  ; screen_byte |= pixel_mask
  lda table_screenbits,y  ; get pixel mask from x(low)
  ldy #0
  ora (ptr_table_idx),y
  sta (ptr_table_idx),y ; set pixel

.out:
}

; Decrements the current px screen row (y0)
!macro _GFXHIRES_LINE_DEC_ROW(adv_h) {
  ; check that 0 <= y0 < 200
  dex ; y0 -= 1
  cpx #GFXHIRES_HEIGHT
  bcc +
    lda adv_h
    bne +
      ; no advance in x-direction (adv_h == 0) -> break
      rts
  $:
}

; in:         x0, x1, y1
; out:        dx, dy, err, draw_loop_idx, ix, iy, adv_h, isSteep ; out
; jump-marks: pset, inc_x0
;
!macro _GFXHIRES_LINE_INIT(x0, x1, y1, dx, dy, err, draw_loop_idx, ix, iy, adv_h, isSteep, pset, inc_x0) {
  ; ix = draw_loop_idx(low) = 1
  ldy #1
  sty ix
  sty draw_loop_idx

  ; draw_loop_idx(high) = isSteep = adv_h = 0
  dey
  sty draw_loop_idx+1
  sty isSteep
  sty adv_h

  ; iy = -1 (255)
  dey
  sty iy

  ; x0(high) >= x1(high) ? !x1_ge_x0+ : !x1_lt_x0+
  lda x1+1 ; x1(high)
  cmp x0+1
  bcc .x1_lt_x0
    bne .x1_ge_x0
  ; x1(high) == x0(high) -> compare x0(low) and x1(low)
  lda x1
  cmp x0
  bcs .x1_ge_x0
    .x1_lt_x0: ; x1 < x0
      ; -> draw direction is right-to-left
      sty ix ; ix = -1 (255)
      MEM_SUBTRACT_UINT16(dx, x0, x1) ; dx = x0 - x1
      jmp .d_y
  .x1_ge_x0: MEM_SUBTRACT_UINT16(dx, x1, x0) ; dx = x1 - x0

  ; calculate dy
  .d_y: MEM_SUBTRACT_FROM_X(dy, y1)  ; dy = y0 - y1

  ; dx < 256 ? bh_loop_init : skip
  lda dx+1
  bne inc_x0
    lda dx ; dx(low)
    cmp dy
    bcs .dx_ge_dy
      sty isSteep ; isSteep = true (255)
      ldy dy
      sty dx
      sta dy ; swap dx(low) and dy
      lda ix
      sta adv_h ; adv_h = ix (1 or -1)
      lda #0
      sta ix
      sta iy ; ix = iy = 0
    .dx_ge_dy:
      ; dx represents the "faster direction"
      SHR16(err, dx, 1) ; err = dx/2

    jmp pset ; directly set first px
}

; Draws a line on hires screen using the Bresenham algorithm.
;
; All macro parameters are given by address.
;
; parameters (const):
; draw_mode    contains a DrawMode enum
;
; tables (start addresses):
; table_mm, table_screenrows_low, table_screenrows_high, table_screenbits
;
; (Their values will be changed by the algorithm.)
;
; screen coordinates (need initial values):
; x0    16bit
; y0
; x1    16bit
; y1
;
; internal vars:
; ptr_table_idx_0   16bit   table index (pointer)
; err               16bit   error variable
; dx                16bit
; dy
; isSteep                   dy > dx ? true : false
; adv_h                     horizontal advance (drawing direction) -> 0/1 (x0<=x1) /  255(x0>x1)
; ix                16bit
; iy
; av0                       bool value (-> DrawMode3d)
;
!macro GFXHIRES_DRAW_LINE(x0, y0, x1, y1, dx, dy, isSteep, adv_h, ix, iy, av0, draw_mode, draw_loop_idx, err, ptr_table_idx_0, table_mm, table_screenrows_low, table_screenrows_high, table_screenbits) {
  ; y0 < y1 ? swap : skip
  ; IMPORTANT: From this point xreg represents y0 throughout the algorythm
  ldx y0 ; xreg is "const" now
  cpx y1
  bcs .n0 ; FIXME: no special labels in macros
    ; swap coordinates
    txa
    ldx y1
    sta y1
    ;stx y0 -> xreg represents y0 (y0 is now "invalid")
    swpy16(x0, x1)
  .n0: ; FIXME: no special labels in macros

  !ifdef CLIP_PX {
    ; nothing to do
  } !else {
    ; TODO: clip line if partly in screen
    ; WORKAROUND: do not draw line if (partly) out-of-screen
    _GFXHIRES_LINE_SCREENBOUNDS(x0,x1)
  }

  ; init dx, dy and ix (draw x-direction)
  _GFXHIRES_LINE_INIT(x0, x1, y1, dx, dy, err, draw_loop_idx, ix, iy, adv_h, isSteep, .pset, .inc_x0)

  .bh_loop: ; bresenham loop
    lda ix ; ix == -1, 0 or 1
    beq .check_x0_high
      bpl .inc_x0
        ; ix == -1
        DEC16(x0) ; x0 -= 1
        jmp .check_x0_high
      .inc_x0:
        ; ix == 1
        INC16(x0) ; x0 += 1
    .check_x0_high:
      !ifdef CLIP_PX {
      } !else {
        ; WORKAROUND: required to tame x coordinate
        lda x0+1
        cmp #$02
        bcc .n1
          rts
        .n1:
      }
    .check_is_steep:
      lda isSteep
      beq .n2
        dex ; y0 -= 1
        cpx y1
        bcs .n2
          rts
      .n2:
    MEM_ADD(err, err, dy) ; err += dy
    ADD_CARRY(err+1)

    .check_err_le_dx:
      ; err < dx ? pset : advance
      lda err+1
      cmp dx+1
      bcc .pset
        bne .advance
        lda err
        cmp dx
        bcc .pset
          beq .pset
          .advance:
            _GFXHIRES_LINE_ADVANCE(adv_h,iy,x0)
            MEM_SUBTRACT_UINT16(err, err, dx) ; err -= dx

    .pset: _GFXHIRES_LINE_PSET(x0, draw_mode, av0, ptr_table_idx_0, table_mm, table_screenrows_low, table_screenrows_high, table_screenbits)

  .bh_next:
    inc draw_loop_idx
    bne .n3
      inc draw_loop_idx+1
    .n3:
    ; dx > draw_loop_idx ? break : jmp next_pixel
    lda draw_loop_idx+1
    cmp dx+1
    bcc .next_pixel
      bne .break
      lda dx
      cmp draw_loop_idx
      bcs .next_pixel
        .break: rts
    .next_pixel: jmp .bh_loop
}

!ifdef GFXHIRES_DBG {
; testing macros
; !include "gfxhires_dbg.asm"
}
