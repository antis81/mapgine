;  Graphics engine for HeightMap drawing
;
; Note on hidden points:
; Hidden point detection relies on z-ordered drawing (front-to-back / bottom-to-top).
;
; Notes on RAM layout:
;  - $801 - $8ff -> upstart code
;  - $0900-$8fff -> reserved for code
;  - $c000-$c3ff -> color ram
;  - $c400-$cfff -> reserved for other runtime tables
;  - $e000-$ff3f -> video ram

!script "macros/three_d.lua"

!include "macros/asm_extensions.asm"
!include "macros/mem.asm"
!include "memmap.asm"

!ifdef HEIGHTMAP {
  !include "macros/heightmap.asm"
}

!include "macros/gfxhires.asm"

!ifdef BASIC_API {
  !include "macros/basic_rom.asm"
}

; Set the draw mode.
; - 0: clear
; - 1: draw
; - 2: draw with "hidden-point detection"
;
; BASIC: poke 832,[0..2]
!macro SET_DRAW_MODE(mode) {
  lda #mode
  sta vars.draw_mode
}

!macro SET_BORDER_COLOR(color) {
  lda #color
  sta VIC.BORDER_COLOR
}

; BASIC upstart
!section "upstart", start=MEM.BASIC_RAM, size=MEM.PAGE_SIZE-1
_start_of_file
upstart: !byte $0b,$08,$00,$00,$9e,str(code)," MAPGINE BY ANTIS",$00,$00,$00
  .end = *-1

; actual start at $900 (2304)
!section "code", start=$900
code
  .size = code_end - code+1

; BASIC API (JUMP-TABLE)
!section "basic_api", in="code", start=*
basic_api:
  !print "---- BASIC API ----"
  !ifdef BASIC_API {
    .prg_init:    MEM_SYS_JMP(prg_init      , "[prg_init      ] graphics initialisation")
    .hires_mode:  MEM_SYS_JMP(vic.mode.hires, "[vic.mode.hires] switch to hires mode")
    .text_mode:   MEM_SYS_JMP(vic.mode.text , "[vic.mode.text ] switch to text mode")
    .gfx_clear:   MEM_SYS_JMP(gfx_clear     , "[gfx_clear     ] clears ~8k graphics screen")
    !ifdef BASIC_FAC {
      .fac_init:  MEM_SYS_JMP(fac_init       , "[fac_init  ] init BASIC float constants (e.g. screen geometry)")
      .draw_line: MEM_SYS_JMP(basic_draw_line, "[draw_line ] draw line (x0,y0,x1,y1)")

      .expect_number: ; expect next "sys" parameter to be a number
        BASIC_NEXT_NUM()

      .uint8_to_float_func:
        ; result is stored in $0d (== x0)
        stx $0d
        BASIC_UINT8_TO_FLOAT_FUNC()

      .fac_to_var_func: ; FAC to 16-bit var
        BASIC_FAC_TO_VAR_FUNC()

      .fac_init: ; initialize float constants
        ; required for calculation from BASIC screen coordinates
        BASIC_UINT8_TO_FLOAT(0, uint8_to_float_func)
          BASIC_FAC_TO_VAR(f_zero, fac_to_var_func) ; screen top/left
        BASIC_UINT8_TO_FLOAT(GFXHIRES_HEIGHT, uint8_to_float_func)
          BASIC_FAC_TO_VAR(f_screen_bottom, fac_to_var_func) ; screen height
        BASIC_ADD_FLOAT_UINT8(f_screen_width, 160, 160, fac_to_var_func, uint8_to_float_func) ; screen width (160 + 160 = 320)
        rts
    }
  }
  !ifdef GFXHIRES_DBG {
    .dbg_lines: MEM_SYS_JMP(dbg_lines              , "[dbg_lines] debugging (-define GFXHIRES_DBG)")
  }
  !ifdef HEIGHTMAP {
    .heightmap_init:   MEM_SYS_JMP(heightmap_init  , "[heightmap_init  ] initialize heightmap rendering")
    .heightmap_render: MEM_SYS_JMP(heightmap_render, "[heightmap_render] refresh the heightmap")
  }
  !print "-------------------"
  !print ""

!section "routines", in="code", start=*
!ifdef BASIC_API {
  prg_init: BASIC_SET_END(basic_ram.end) ; reduce basic memory
} !else {
  !print "*** ASSEMBLER MODE! NO BASIC BYTES FREE! ***"
  !include "macros/math.asm"
  mul:
    .square0_lo = r_tables.math_square0_lo ; table requires 512 bytes RAM
    .square0_hi = r_tables.math_square0_hi ; table requires 512 bytes RAM
    .square1_lo = r_tables.math_square1_lo ; table requires 512 bytes RAM
    .square1_hi = r_tables.math_square1_hi ; table requires 512 bytes RAM
    .f0         = _tmp16_0 ; factor 0
    .f1         = _tmp16_1 ; factor 1
    .result     = _tmp32_0 ; product

  mul_init:
      MATH_MAKE_MULTAB16(mul.square0_hi, mul.square0_lo, mul.square1_hi, mul.square1_lo)
      rts
  ; mul_uint8_: MATH_UINT8_FASTMUL_X(_tmp8_0) ; no tables (~140 cycles)
  mul_uint8:  MATH_UINT8_MUL_X(mul.f0, mul.f1, mul.result, mul.square0_hi, mul.square0_lo, mul.square1_hi, mul.square1_lo)
  mul_int8:   MATH_INT8_MUL_X(mul.f0, mul.f1, mul.result, mul_uint8)
  ; mul_uint16: MATH_UINT16_MUL_X()
  ; mul_int16:  MATH_INT16_MUL_X()
}

; !section "vic", in="routines", start=*
vic:
  !include "macros/vic.asm"

  .mode_text:
    VIC_SELECT_MODE_TEXT()

    ; relocate vram (default)
    lda #$15 ; %0001 0101
    VIC_SET_VRAM()

    lda #$97 ; %1001 0111
    jmp ._set_bank

  .mode_hires:
    VIC_SELECT_MODE_HIRES()

    ; relocate vram ($e000-$ff40)
    lda #$0d ; %0000 1011
    VIC_SET_VRAM()

    lda #$3c ; %0011 1100
    ;jmp ._set_bank

  ._set_bank: VIC_SET_BANK()

    lda #$c8 ; %1100 1000
    VIC_SET_MODE()
    rts

; !section "gfx", in="routines", start=*
gfx_set_colors:
  GFXHIRES_SET_COLORS(color_ram)
  rts
gfx_clear:
  lda #$00 ; fillbyte
  GFXHIRES_CLEAR(vram)
  lda vars.draw_mode
  cmp #DrawMode.ThreeD
  bne +
    GFXHIRES_CLEAR_TABLE_MM(r_tables.gfx_mmtab, _tmp8_0)
  $: rts

draw_line: ; line drawing routine
  !ifdef HEIGHTMAP {
    HEIGHTMAP_TRANSFORM_LINE(mul.f0, mul.f1, mul.result, s_tables.three_d_y_fact, mul_int8, mul_uint8)
  } !else {
    ; INIT_LINE_VALUES
    lda vars.x0
    sta _x0
    lda vars.x0+1
    sta _x0+1

    lda vars.x1
    sta _x1
    lda vars.x1+1
    sta _x1+1

    lda vars.y0
    sta _y0

    lda vars.y1
    sta _y1
  }
  GFXHIRES_DRAW_LINE(_x0,_y0,_x1,_y1, _dx,_dy,_line_is_steep,_adv_h,_ix,_iy, _tmp8_0, _draw_mode, _bh_loop_idx, _bh_err, _ptr0, r_tables.gfx_mmtab, s_tables.gfxhires_rows_lo, s_tables.gfxhires_rows_hi, s_tables.gfxhires_bits)

!ifdef BASIC_FAC {
  ; check (float) coords are in screen area
  basic_check_line_coords: ; check 0 <= f_x0 <= f_x1 < 320
    BASIC_VAR_TO_FAC(f_x0)
    BASIC_FAC_CMP(f_x1)
    bmi + ; ok
    jsr basic_swap_coords ; x0 > x1
  $: ; check 0 <= f_x0 (screen left)
    BASIC_VAR_TO_FAC(f_zero)
    BASIC_FAC_CMP(f_x0)
    bpl + ; ok
    jmp check_f_x1_lt_320
  $: ; check 0 < f_x1 (screen right)
    BASIC_FAC_CMP(f_x1)
    bmi +
    jmp basic_coords_out_of_screen
  $:
    ; temp = (y1 - y0) * x1
    BASIC_VAR_TO_FAC(f_y0)
    BASIC_FAC_SUB(f_y1)
    BASIC_FAC_MUL(f_x1)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; y0 = y1 - (temp / (x1 - x0))
    BASIC_VAR_TO_FAC(f_x0)
    BASIC_FAC_SUB(f_x1)
    BASIC_FAC_DIV(f_temp)
    BASIC_FAC_SUB(f_y1)
    BASIC_FAC_TO_VAR(f_y0, fac_to_var_func)

    BASIC_VAR_TO_FAC(f_zero)
    BASIC_FAC_TO_VAR(f_x0, fac_to_var_func)
  check_f_x1_lt_320:
    ;nop
    BASIC_VAR_TO_FAC(f_screen_width)
    BASIC_FAC_CMP(f_x1)
    bmi + ; ok
    jmp check_f_y1_lt_f_y0
  $: ; check x0 < 320
    ;nop
    BASIC_FAC_CMP(f_x0)
    bpl + ; ok
    jmp basic_coords_out_of_screen
  $:
    ;nop
    ; temp = 320 - x0
    BASIC_VAR_TO_FAC(f_x0)
    BASIC_FAC_SUB(f_screen_width)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; temp *= (y0 - y1)
    BASIC_VAR_TO_FAC(f_y1)
    BASIC_FAC_SUB(f_y0)
    BASIC_FAC_MUL(f_temp)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; y1 = y0 - (temp / (x1 - x0))
    BASIC_VAR_TO_FAC(f_x0)
    BASIC_FAC_SUB(f_x1)
    BASIC_FAC_DIV(f_temp)
    BASIC_FAC_SUB(f_y0)
    BASIC_FAC_TO_VAR(f_y1, fac_to_var_func)

    ; x1 = f_screen_width
    BASIC_VAR_TO_FAC(f_screen_width)
    BASIC_FAC_TO_VAR(f_x1, fac_to_var_func)
  check_f_y1_lt_f_y0: ; ensure bottom-to-top drawing
    ;nop
    BASIC_VAR_TO_FAC(f_y0) ; y0
    BASIC_FAC_CMP(f_y1) ; y1
    bmi + ; ok
    jsr basic_swap_coords
  $: ; check f_y0 > 0
    ;nop
    BASIC_VAR_TO_FAC(f_zero)
    BASIC_FAC_CMP(f_y0)
    cmp #1
    beq +
      jmp check_f_y1_lt_200
  $:
    ;nop
    BASIC_FAC_CMP(f_y1)
    cmp #1
    bne +
      jmp basic_coords_out_of_screen
  $:
    ;nop
    ; temp = x1 - x0
    BASIC_VAR_TO_FAC(f_x0)
    BASIC_FAC_SUB(f_x1)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; temp *= y1
    BASIC_VAR_TO_FAC(f_y1) ; VAR_TO_FAC(f_zero); FAC_SUB(f_y1)
    BASIC_FAC_MUL(f_temp)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; x0 = x1 - (temp / (y1-y0))
    BASIC_VAR_TO_FAC(f_y0)
    BASIC_FAC_SUB(f_y1)
    BASIC_FAC_DIV(f_temp)
    BASIC_FAC_SUB(f_x1)
    BASIC_FAC_TO_VAR(f_x0, fac_to_var_func)
    ; y0 = 0
    BASIC_VAR_TO_FAC(f_zero)
    BASIC_FAC_TO_VAR(f_y0, fac_to_var_func)
  check_f_y1_lt_200: ; check f_y1 < 200
    ;nop
    BASIC_VAR_TO_FAC(f_screen_bottom)
    BASIC_FAC_CMP(f_y1)
    bmi +
    jmp basic_coords_in_screen
  $: ;
    ;nop
    BASIC_FAC_CMP(f_y0)
    bpl +
    jmp basic_coords_out_of_screen
  $:
    ;nop
    ; temp = 200 - y0
    BASIC_VAR_TO_FAC(f_y0)
    BASIC_FAC_SUB(f_screen_bottom)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; temp *= (x0-x1)
    BASIC_VAR_TO_FAC(f_x1)
    BASIC_FAC_SUB(f_x0)
    BASIC_FAC_MUL(f_temp)
    BASIC_FAC_TO_VAR(f_temp, fac_to_var_func)
    ; x1 = x0 - (temp / (y1-y0))
    BASIC_VAR_TO_FAC(f_y0)
    BASIC_FAC_SUB(f_y1)
    BASIC_FAC_DIV(f_temp)
    BASIC_FAC_SUB(f_x0)
    BASIC_FAC_TO_VAR(f_x1, fac_to_var_func)
    ; y1 = 199
    BASIC_VAR_TO_FAC(f_screen_bottom)
    ldy #>f_y1; lda #<f_y1
  basic_coords_in_screen:
    jsr fac_to_var_func
    lda #1
    sta result_code
    rts

  basic_coords_out_of_screen:
    lda #0
    sta result_code
    rts

  basic_swap_coords:
    BASIC_SWAP_FLOAT(f_x0, f_x1) ; swap x0 <-> x1
    BASIC_SWAP_FLOAT(f_y0, f_y1) ; swap y0 <-> y1
    rts

  basic_draw_line:
    jsr basic_expect_number; BASIC_FAC_TO_VAR(f_x0, fac_to_var_func) ; get x0
    jsr basic_expect_number; BASIC_FAC_TO_VAR(f_y0, fac_to_var_func) ; get y0
    jsr basic_expect_number; BASIC_FAC_TO_VAR(f_x1, fac_to_var_func) ; get x1
    jsr basic_expect_number; BASIC_FAC_TO_VAR(f_y1, fac_to_var_func) ; get y1

    jsr basic_check_line_coords
    beq +

    BASIC_VAR_TO_FAC(f_y0) ; y0 (f_y0) to FAC
    BASIC_FAC_TO_INT16(); mov ZP14_BASIC : y0

    BASIC_VAR_TO_FAC(f_y1) ; y1 (f_y1) to FAC
    BASIC_FAC_TO_INT16()
    lda ZP14_BASIC
    sta y1

    BASIC_VAR_TO_FAC(f_x1) ; x0 (f_x1) to FAC
    BASIC_FAC_TO_INT16()
    lda ZP14_BASIC
    sta x1	  ; x1(low)
    lda ZP14_BASIC+1
    sta x1+1 ; x1(high)

    BASIC_VAR_TO_FAC(f_x0) ; x1 (f_x0) to FAC
    BASIC_FAC_TO_INT16()

    GFXHIRES_BEGIN_DRAW_CONTEXT(addr_gfx_ram)
    jsr draw_line
    GFXHIRES_END_DRAW_CONTEXT(addr_gfx_ram)
  $:
    rts
}

!section "main", in="code", start=*
!ifdef GFXHIRES_DBG {
  dbg_lines: ; draw triangular test pattern with a number of lines
    SET_BORDER_COLOR(LIGHT_RED)
    SET_DRAW_MODE(DrawMode.Set)
    jsr vic.mode.hires
    GFXHIRES_BEGIN_DRAW_CONTEXT(addr_gfx_ram)
    _INIT_DRAW_MODE()
    GFXHIRES_RUN_TESTS(vars.x0, vars.y0, vars.x1, vars.y1, draw_line, gfx_clear, 200)
    GFXHIRES_END_DRAW_CONTEXT(addr_gfx_ram)
    SET_BORDER_COLOR(LIGHT_BLUE)
    jmp vic.mode.text
}

!ifdef HEIGHTMAP {
  heightmap_init:
    jsr mul_init ; init math multiplication tables
    SET_DRAW_MODE(DrawMode.Set)
    ;HEIGHTMAP_INIT(heightmap_set_field_width)
    jsr vic.mode_hires

    .fg = $f
    .bg = $b
    .hiresColors = $fb ; FIXME: (.fg << 4) | .bg
    lda #.hiresColors
    jsr gfx_set_colors

  heightmap_render:
    SET_BORDER_COLOR(LIGHT_GREEN) ; debugging
    _INIT_DRAW_MODE()
    GFXHIRES_BEGIN_DRAW_CONTEXT(vram)
    lda #200
    sta _tmp8_0 ; do not draw "inside" mountains
    jsr gfx_clear
    ; NOTE: Changing parameters can be expensive -> use const values for now
    ;       only needed if a parameter was changed (scaling, rotating, etc.)
    ; jsr heightmap_set_field_width
    HEIGHTMAP_DRAW(s_tables.heightmap_rows_lo, s_tables.heightmap_rows_hi, s_tables.three_d_y_fact, draw_line)
    GFXHIRES_END_DRAW_CONTEXT(vram)
    SET_BORDER_COLOR(LIGHT_BLUE) ; debugging
    !ifdef HM_NO_ENGINE {
      ; usable for testing
      rts
    } !else {
      ; (poor man's) engine loop
      ; TODO: sync rendering with raster interrupt
      jmp heightmap_render
    }

  ; heightmap subroutines
  ; heightmap_set_field_width: HEIGHTMAP_SET_FIELD_WIDTH(mul_uint8); rts
}

code_end = *-1  ; marks the end of "code" section

; TABLES
!align $100
!section "s_tables", start=*
!print MemArrayStr("s_tables", s_tables, s_tables.end)
; !print "s_tables:  ", s_tables, "-", s_tables.end, " (", str(s_tables.size), " bytes)"
s_tables:
  ; 256 bytes (76 bytes filled)
  .sin: !fill MathMakeSin(32, 180)
  .sin_end = *-1
  MEM_FILL_PAGE(180)
  !print "  ", MemArrayStr("s_tables.sin", .sin, .sin_end)
  ; !print "  - values -> ", .sin

  ; 256 bytes (76 bytes filled)
  .cos: !fill MathMakeCos(32, 180)
  .cos_end = *-1
  MEM_FILL_PAGE(180)
  !print "  ", MemArrayStr("s_tables.cos", .cos, .cos_end)
  ; !print "  - values -> ", .sin

  ; // 256 bytes
  .three_d_y_fact_factors = ThreeD_MakeProjectionFactors(280.0, 5.0)
  .three_d_y_fact_min = ThreeD_ProjectionFactorsMin(.three_d_y_fact_factors)
  .three_d_y_fact_max = ThreeD_ProjectionFactorsMax(.three_d_y_fact_factors)
  .three_d_y_fact: !fill .three_d_y_fact_factors
  .three_d_y_fact_end = *-1
  !print "  ", MemArrayStr("s_tables.three_d_y_fact", .three_d_y_fact, .three_d_y_fact_end)
  !print "    - range: [", str(.three_d_y_fact_min), "..", str(.three_d_y_fact_max), "]"

  .gfxhires_bits: GFXHIRES_TABLE_BITS() ; 256 bytes
  .gfxhires_bits_end = *-1
  !print "  ",MemArrayStr("s_tables.gfxhires_bits", s_tables.gfxhires_bits, s_tables.gfxhires_bits_end)
  .gfxhires_rows: ; 400 bytes
  .gfxhires_rows_lo: GFXHIRES_TABLE_SCREENROW_LOW()              ; 200 bytes (lo)
  .gfxhires_rows_hi: GFXHIRES_TABLE_SCREENROW_HIGH(vram) ; 200 bytes (hi)
  .gfxhires_rows_end = *-1
  !print "  ",MemArrayStr("s_tables.gfxhires_rows", .gfxhires_rows, .gfxhires_rows_end)

  !ifdef HEIGHTMAP {
    .heightmap_rows:
    ; 64 bytes -> heightmap.width
    .heightmap_rows_lo: !rept heightmap.width {
          .addr = heightmap + (i*heightmap.width)
          !byte <.addr
        }
    ; 64 bytes -> heightmap.width
    .heightmap_rows_hi: !rept heightmap.width {
      .addr = heightmap + (i*heightmap.width)
      !byte >.addr
    }
    .heightmap_rows_end = *-1
    !print "  ",MemArrayStr("s_tables.heightmap_rows", .heightmap_rows, .heightmap_rows_end)
  }

  .end = *-1
  .size = .end - s_tables + 1


; EOF information (no code generation beyond this point)
_eof = *-1 ; create _eof label on last created byte
  !print "Code    ", code, "-", code_end, " (", str(code.size), " bytes)"

  ; EOF
  .size = _eof - _start_of_file
  !print "File    ",_start_of_file,"-",_eof," (",str(.size)," bytes)"
  !print ""
